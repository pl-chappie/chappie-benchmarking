#!/bin/bash

benchmarks=(
avrora
batik
h2
jython
sunflow
)

reference_dir=./processed_data
coapp_dir=./scripting/processed_foreign

echo "combination,  app,  pearson,  spearman" > ./scripting/foreign.rank

for benchmark in "${benchmarks[@]}"; do
  echo $benchmark
  exprdir=${benchmark}_ferret

  echo "$reference_dir/graphs/$benchmark/filtered_attribution_ranking.csv"
  echo "$coapp_dir/$exprdir/graphs/$benchmark/filtered_attribution_ranking.csv"

  /usr/bin/python3.5 ./scripting/method-ranking.py -reference="$reference_dir/graphs/$benchmark/filtered_attribution_ranking.csv" -coapp="$coapp_dir/$exprdir/graphs/$benchmark/filtered_attribution_ranking.csv" -resultfile="./scripting/foreign.rank" -tag="$exprdir,  $benchmark"

  echo "" >> ./scripting/foreign.rank
 
done
