#!/bin/bash

echo "Running Benchmarks $1 - $2"

instance_a="chappie_a_$1"
instance_b="chappie_b_$2"

if [ -d "${instance_a}" ]
then
	rm -rf "${instance_a}"
fi


if [ -d "${instance_b}" ]
then
	rm -rf "${instance_b}"
fi

mkdir "${instance_a}"
mkdir "${instance_b}"

export MODE=NOP

export POLLING=4
export ITERS=1
export CORE_RATE=10
export READ_JIFFIES=true

echo "Execution $0 ..."
file_dir=`dirname "$0"`
#${file_dir}/../chappie/run/run.sh bench_jars/dacapo-9.12-bach.jar "" Harness "-no-validation ${benchmarks[1]}"

pwd
cp /proc/stat start
cd "${instance_a}"
../../chappie/run/run.sh ../dacapo-9.12-bach.jar "" Harness "-no-validation $1" &
first_id=$!
cd ../
cd "${instance_b}"
../../chappie/run/run.sh ../dacapo-9.12-bach.jar "" Harness "-no-validation $2" &
second_id=$!
cd ../
cp /proc/stat end
tail --pid=${first_id} -f /dev/null
tail --pid=${second_id} -f /dev/null

if [ -d "$1_$2" ]
then
	rm -r "$1_$2"
fi
mkdir $1_$2

mv "${instance_a}" "$1_$2"
mv "${instance_b}" "$1_$2"
cp start "$1_$2"
cp end "$1_$2"
