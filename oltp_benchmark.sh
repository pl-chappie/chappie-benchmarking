#!/bin/bash

rm -rf chappie.oltpbench*

mkdir chappie.oltpbench
mkdir chappie.oltpbench.NOP
mkdir chappie.oltpbench.SAMPLE1
mkdir chappie.oltpbench.JIFFIES1
mkdir chappie.oltpbench.SAMPLE10
mkdir chappie.oltpbench.JIFFIES10


benchmarks=(
tatp
tpcc
tpch
noop
smallbank
twitter
ycsb
epinions
)

export ITERS=1
export MODE=NOP
export POLLING=4
export CORE_RATE=10
export READ_JIFFIES=false

iter=0
for benchmark in "${benchmarks[@]}"; do
  mkdir chappie.oltpbench/${benchmark}
  for i in $(seq 0 $iter); do
    ../chappie/run/run.sh oltp.jar "" com.oltpbenchmark.DBWorkload  " -b ${benchmark} -c config/sample_${benchmark}_config.xml --execute=true -s 5 -o ${benchmark}.out" "-Dlog4j.configuration=log4j.properties"
    mkdir $benchmark
    mv chappie.oltp/* $benchmark

    for file in ${benchmark}/*.*.0.csv; do
      mv $file ${file%.0.csv}.$i.csv
    done

    for file in ${benchmark}/*.*.0.txt; do
      mv $file ${file%.0.txt}.$i.txt
    done

    mv ${benchmark}/log.hpl ${benchmark}/log.${i}.hpl

    mkdir chappie.oltp.NOP/${benchmark}
    mv $benchmark/* chappie.oltp.NOP/${benchmark}/.
    rm -rf $benchmark
  done
done

export MODE=VM_SAMPLE
for benchmark in "${benchmarks[@]}"; do
  mkdir chappie.oltpbench/${benchmark}
  for i in $(seq 0 $iter); do
    ../chappie/run/run.sh oltp.jar "" com.oltpbenchmark.DBWorkload  " -b ${benchmark} -c config/sample_${benchmark}_config.xml --execute=true -s 5 -o ${benchmark}.out" "-Dlog4j.configuration=log4j.properties"
    mkdir $benchmark
    mv chappie.oltp/* $benchmark

    for file in ${benchmark}/*.*.0.csv; do
      mv $file ${file%.0.csv}.$i.csv
    done

    for file in ${benchmark}/*.*.0.txt; do
      mv $file ${file%.0.txt}.$i.txt
    done

    mv ${benchmark}/log.hpl ${benchmark}/log.${i}.hpl

    mkdir chappie.oltp.SAMPLE/${benchmark}
    mv $benchmark/* chappie.oltp.SAMPLE/${benchmark}/.
    rm -rf $benchmark
  done
done

rm -rf chappie.oltpbench
mkdir chappie.oltpbench
mv chappie.oltp.* chappie.oltp/.

rm scratch -rf
