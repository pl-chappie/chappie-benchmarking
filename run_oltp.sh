#!/bin/bash

if [ -d chappie.oltp ]
then
  rm -rf chappie.oltp
fi

mkdir chappie.oltp

export MODE=VM_SAMPLE
export ITERS=10
export STACK_PRINT=true
export MEMORY=false
export POLLING=4

benchmarks=(
#tatp
#tpcc
#tpch
#noop
#smallbank
twitter
#ycsb
#epinions
)

#file_dir=`dirname "$0"`
#${file_dir}/../chappie/run/run.sh bench_jars/dacapo-9.12-bach.jar "" Harness "-no-validation ${benchmarks[1]}"
for benchmark in "${benchmarks[@]}"; do
  echo "Processing $benchmark"

  ../chappie/run/run.sh oltp.jar "" com.oltpbenchmark.DBWorkload  " -b ${benchmark} -c config/sample_${benchmark}_config.xml --execute=true -s 5 -o ${benchmark}.out" "-Dlog4j.configuration=log4j.properties"

  mkdir $benchmark
  mv chappie.oltp/*.csv $benchmark
  mv chappie.oltp/*.hpl $benchmark
done


for benchmark in "${benchmarks[@]}";
do
  mv $benchmark chappie.oltp/$benchmark
done
