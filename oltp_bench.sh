#!/bin/bash
export MODE=3
export ITERS=1

#rm chappie.oltp/*

echo "Processing $1" >> oltp_progress

../chappie/run/run.sh oltp.jar "" com.oltpbenchmark.DBWorkload  " -b $1 -c config/sample_$1_config.xml --execute=true -s 5 -o ${benchmark}.out" "-Dlog4j.configuration=log4j.properties"
  
      
   if [ -d $1 ]
   then
      	rm -rf $1
   fi

  
   echo "Creating dir $1" >> oltp_progress 
   echo "mkdir $1" >> oltp_progress
   mkdir $1
   mv chappie.oltp/*.csv $1
   mv $1 chappie.oltp/$1
   echo "$1 Done" >> oltp_progress
