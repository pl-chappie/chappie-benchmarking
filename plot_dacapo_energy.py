import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import kutils
from matplotlib.backends.backend_pdf import PdfPages

 # set width of bar
barWidth = .1

benchmark_records=kutils.read_csv_as_array("energy.data")
#Each record represents a benchmark run
#The columns are ordered as : benchmark name, instance one energy, instance two energy, other apps

benchmark_names=[]
instance_a=[]
instance_b=[]
others=[]
#row_1=[]
#row_2=[]
both=[]

i=0
while i < len(benchmark_records):
    record = benchmark_records[i]
    if(record[0].startswith("#")):
        i=i+1
        continue
    benchmark_names.append(record[1]+"-"+record[3])
    instance_a.append(float(record[2]))
    instance_b.append(float(record[4]))
    others.append(float(record[6]))
    #row_1.append(float(record[8]))
    #row_2.append(float(record[10]))
    both.append(float(record[2])+float(record[4]))
    i=i+1



# Set position of bar on X axis
r1 = np.arange(len(instance_a))
r2 = [x + barWidth for x in r1]
r3 = [x + barWidth for x in r2]
r4 = [x + barWidth for x in r3]
#r4 = [x + barWidth for x in r3]
#r5 = [x + barWidth for x in r4]
    
# Make the plot
plt.bar(r1, instance_a, color='#7f6d5f', width=barWidth, edgecolor='white',label="App A")
plt.bar(r2, instance_b, color='#557f2d', width=barWidth, edgecolor='white',label="App B")
plt.bar(r3, others,     color='#2d7f5e', width=barWidth, edgecolor='white',label="Others")
plt.bar(r4, both,       color='#dd756e', width=barWidth, edgecolor='white',label="A+B")
#plt.bar(r4, row_1,     color='#dd756e', width=barWidth, edgecolor='white',label="Raw A")
#plt.bar(r5, row_2,     color='#fff111', width=barWidth, edgecolor='white',label="Raw B")
     
     # Add xticks on the middle of the group bars
plt.xlabel('Benchmarks', fontweight='bold')
plt.ylabel('Energy in Jouls', fontweight='bold')
plt.xticks([r + barWidth for r in range(len(instance_a))], benchmark_names)
pp = PdfPages('figure.pdf')      
# Create legend & Show graphic
plt.legend()
plt.show()
plt.plot()
#pp.savefig(plt)
plt.savefig("energy.png", dpi=1000)

#f.savefig("foo.pdf", bbox_inches='tight')
