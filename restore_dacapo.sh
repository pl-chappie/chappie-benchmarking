#!/bin/bash

# usage: Invoke with old to skip the method alignment, invoke with new to perform it:
#   ./analyze_dacapo.sh old
#   ./analyze_dacapo.sh new


benchmarks=(
#twitter
#avrora 
#batik
#eclipse 
#h2 
#jython 
#luindex 
#lusearch 
#pmd 
sunflow 
#tomcat     
#tradebeans 
#tradesoap  
#xalan
)

for benchmark in "${benchmarks[@]}"; do
  nlogs=`find ./chappie.dacapo/${benchmark} -maxdepth 1 -type f -name '*.thread.*.csv' | wc -l`
  nlogs=$((nlogs-1))
  d=./chappie.dacapo/${benchmark}
  for i in `seq 0 $nlogs`; do
    cp $d/saved.$i.csv $d/chappie.thread.$i.csv
  done
done
