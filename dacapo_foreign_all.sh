#! /bin/bash
export ITERS=1
export MODE=VM_SAMPLE
export MEMORY=false
export POLLING=4
export CORE_RATE=10
export READ_JIFFIES=TRUE
iter=9


benchmarks=(
batik
)

#for benchmark in "${benchmarks[@]}"; do
#	mv ${benchmark}_${benchmark} dacapo_x_x
#done
#exit

if [ -d "dacapo_foreign" ]
then
  rm -r dacapo_foreign
fi

mkdir dacapo_foreign

for benchmark in "${benchmarks[@]}"; do
  echo "Runing $benchmark with foreign load"
  bash dacapo_foreign.sh $benchmark
  mv "${benchmark}_ferret" dacapo_foreign_load
done
