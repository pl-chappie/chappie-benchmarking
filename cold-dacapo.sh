#!/bin/bash

rm -rf chappie.dacapo
mkdir chappie.dacapo

export ITERS=1
export MODE=VM_SAMPLE
export MEMORY=false
export POLLING=4
export CORE_RATE=10
export READ_JIFFIES=true

iter=9

benchmarks=(
avrora 
batik
#eclipse 
#h2 
#jython 
#luindex 
#lusearch 
#pmd 
#sunflow 
#tomcat 
#tradebeans 
#tradesoap 
#xalan
)

for benchmark in "${benchmarks[@]}"; do
  mkdir chappie.dacapo/${benchmark}
  for i in $(seq 0 $iter); do
    ../chappie/run/run.sh dacapo-9.12-bach.jar "" Harness "-no-validation ${benchmark}"
    mkdir $benchmark
    mv chappie.dacapo-9.12-bach/* $benchmark

    for file in ${benchmark}/*.*.0.csv; do
      mv $file ${file%.0.csv}.$i.csv
    done

    for file in ${benchmark}/*.*.0.txt; do
      mv $file ${file%.0.txt}.$i.txt
    done

    mv ${benchmark}/log.hpl ${benchmark}/log.${i}.hpl

    mv $benchmark/* chappie.dacapo/${benchmark}/.
    rm -rf $benchmark
  done
done

rm scratch -rf
