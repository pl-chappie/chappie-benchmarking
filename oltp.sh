#!/bin/bash

if [ -d chappie.oltp ]
then
	rm -rf chappie.oltp
fi

mkdir chappie.oltp

export MODE=3
expoert READ_JIFFIES=TRUE
export ITERS=10

benchmarks=(
	tatp
	tpcc
	tpch
	noop
	smallbank
	twitter
	ycsb
	epinions
)

#benchmarks=(
#	"tpcc"
#	"tpch"
#)

#file_dir=`dirname "$0"`
#${file_dir}/../chappie/run/run.sh bench_jars/dacapo-9.12-bach.jar "" Harness "-no-validation ${benchmarks[1]}"
echo "Starting OLTP Benchmarks" > oltp_progress

for benchmark in "${benchmarks[@]}"; do
     echo "Processing $benchmark"
     echo "Processing $benchmark" >> oltp_progress
   
    ../chappie/run/run.sh oltp.jar "" com.oltpbenchmark.DBWorkload  " -b ${benchmark} -c config/sample_${benchmark}_config.xml --execute=true -s 5 -o ${benchmark}.out" "-Dlog4j.configuration=log4j.properties"
  
      
   if [ -d $benchmark ]
   then
      	rm -rf $benchmark
   fi

  
   echo "Creating dir $benchmark" >> oltp_progress 
   echo "mkdir $benchmark" >> oltp_progress
   mkdir $benchmark
   mv chappie.oltp/*.csv $benchmark
   mv chappie.oltp/*.txt $benchmark
   #cp -r $benchmark chappie.oltp/$benchmark
   echo "$benchmark Done" >> oltp_progress
done


for benchmark in "${benchmarks[@]}";
do
   cp -r $benchmark chappie.oltp/$benchmark
done
