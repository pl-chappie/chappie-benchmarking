#!/bin/bash

if [ -d chappie.graphchi ]
then
	rm -rf chappie.graphchi
fi

benchmarks=(
	Pagerank
	ConnectedComponents
	ALSMatrixFactorization
	)


export MODE=3
export ITERS=10
export READ_JIFFIES=TRUE
tmp="graphchi_temp"
datadir="graphchi_data"

for benchmark in "${benchmarks[@]}"; do
	if [ -d "$tmp" ];
	then
		sudo rm -r "$tmp"
	fi

	mkdir "$tmp"
	cp "${datadir}/${benchmark}.data" "${tmp}"
	sudo ../chappie/run/run.sh graphchi.jar "" "edu.cmu.graphchi.apps.${benchmark}" "${tmp}/${benchmark}.data 1"

	if [ -d "${benchmark}" ]
	then
		rm -rf "${benchmark}"
	fi

  	mkdir ${benchmark}

	mv chappie.graphchi/*.csv $benchmark	
	mv chappie.graphchi/*.txt $benchmark

done


for benchmark in "${benchmarks[@]}";
do
   cp -r $benchmark chappie.graphchi/$benchmark
done
