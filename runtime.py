import os
import sys
import numpy as np
import pandas as pd

pd.options.display.float_format = '{:.3f}'.format

iters = 10

data = {}

root = 'chappie.benchmark'

for run in ('NOP', 'VM_SAMPLE8'):
    path = '{}.{}'.format(root, run)
    print(run)
    for bench in ('tradebeans', ): # os.listdir(path):
        f = os.path.join(path, bench)
        if bench not in data:
            data[bench] = {'runtime': [], 'threads': [], 'trace': []}

        data[bench]['runtime'].append(pd.DataFrame({'runtime': [int(open(os.path.join(f, 'chappie.runtime.{}.csv'.format(i))).read()) for i in range(iters)], 'run': [run] * iters}))
        if run != 'NOP':
            [data[bench]['threads'].append(pd.read_csv(os.path.join(f, 'chappie.thread.{}.csv'.format(i)))) for i in range(iters)]
            [data[bench]['trace'].append(pd.read_csv(os.path.join(f, 'chappie.trace.{}.csv'.format(i)))) for i in range(iters)]

final_data = pd.DataFrame()

for bench in data:
    data[bench]['runtime'] = pd.concat(data[bench]['runtime'])
    df1 = (data[bench]['runtime'].groupby('run').mean() / 10**9).reset_index().pivot_table(values = 'runtime', columns = 'run')
    df2 = (data[bench]['runtime'].groupby('run').std() / 10**9).reset_index().pivot_table(values = 'runtime', columns = 'run').rename(columns = {'NOP': 'NOP_STD', 'VM_SAMPLE8': 'VM_SAMPLE_STD'})
    df = pd.concat([df1, df2], axis = 1)

    data[bench]['threads'] = pd.concat(data[bench]['threads'])
    df['TOTAL_THREADS'] = len(data[bench]['threads']['thread'].unique())
    df['RUNNING_THREADS'] = data[bench]['threads'].groupby('epoch')['thread'].count().mean() / iters

    energies = []
    for energy in data[bench]['trace']:
        energy = energy.groupby('socket').max() - energy.groupby('socket').min()
        energies.append(energy[['package', 'dram']].sum())
    energy = pd.concat(energies)

    df['PACKAGE'] = energy[energy.index == 'package'].mean()
    df['DRAM'] = energy[energy.index == 'dram'].mean()
    df['PACKAGE_STD'] = energy[energy.index == 'package'].std()
    df['DRAM_STD'] = energy[energy.index == 'dram'].std()

    df['benchmark'] = bench

    final_data = final_data.append(df)

final_data['VM_SAMPLE_SLOWDOWN'] = (final_data['VM_SAMPLE8'] - final_data['NOP']) / final_data['NOP']
final_data.to_csv('dacapo_runtime_raw.csv', index = False, float_format = '%.3f')

final_data = pd.read_csv('dacapo_runtime_raw.csv')

table = final_data[['benchmark', 'NOP', 'VM_SAMPLE8', 'VM_SAMPLE_SLOWDOWN']]
table.columns = ['benchmark', 'NOP time', 'SAMPLE time', 'overhead']

from tabulate import tabulate
table = tabulate(table, headers = table.columns, showindex = False, tablefmt = 'github')
print(table)
open('tradebeans_runtime.txt', 'w').write(table)
