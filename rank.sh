#!/bin/bash

benchmarks=(
avrora
batik
h2
jython
sunflow
Pagerank
avrora,batik
h2,jython
avrora,jython
sunflow,avrora
h2,sunflow
Pagerank,ConnectedComponents
)

reference_dir=./processed_data
coapp_dir=./scripting/processed_data

echo "combination,  app,  pearson,  spearman" > ./scripting/energy.rank

for benchmark in "${benchmarks[@]}"; do
  echo $benchmark
  if [[ "$benchmark" == *","* ]];
  then
    b1=$(echo $benchmark | cut -d "," -f 1)
    b2=$(echo $benchmark | cut -d "," -f 2)
    exprdir=${b1}_${b2}
    ref1=$b1
    ref2=$b2
  else
    exprdir=${benchmark}_${benchmark}
    b1=${benchmark}_1
    b2=${benchmark}_2
    ref1=$benchmark
    ref2=$benchmark
  fi

  echo "$reference_dir/graphs/$ref1/filtered_attribution_ranking.csv"
  echo "$coapp_dir/$exprdir/graphs/$b1/filtered_attribution_ranking.csv"

  echo "$reference_dir/graphs/$ref2/filtered_attribution_ranking.csv"
  echo "$coapp_dir/$exprdir/graphs/$b2/filtered_attribution_ranking.csv"
  echo ""

  /usr/bin/python3.5 ./scripting/method-ranking.py -reference="$reference_dir/graphs/$ref1/filtered_attribution_ranking.csv" -coapp="$coapp_dir/$exprdir/graphs/$b1/filtered_attribution_ranking.csv" -resultfile="./scripting/energy.rank" -tag="$exprdir,  $b1"

  /usr/bin/python3.5 ./scripting/method-ranking.py -reference="$reference_dir/graphs/$ref2/filtered_attribution_ranking.csv" -coapp="$coapp_dir/$exprdir/graphs/$b2/filtered_attribution_ranking.csv" -resultfile="./scripting/energy.rank" -tag="$exprdir,  $b2"

  echo "" >> ./scripting/energy.rank
 
done
