#!/bin/bash

#PATH="./fast_rate_data"
#DEST="fast_rate_data"

PATH="./processed_data"
DEST="processed_data"


benchmarks=(
#avrora  
#batik
#eclipse 
#fop
#h2 
#jython  
#luindex 
#lusearch 
#pmd 
#sunflow 
#tomcat     
#tradebeans 
#tradesoap 
#xalan
#ALSMatrixFactorization
#ConnectedComponents
#Pagerank
#twitter
#tpcc
smallbank
)

group1=(
avrora  
batik
eclipse 
fop
h2 
jython  
luindex 
lusearch 
pmd 
sunflow 
tomcat     
tradebeans 
tradesoap 
xalan
)

group2=(
ALSMatrixFactorization
ConnectedComponents
Pagerank
twitter
tpcc
smallbank
)

all_group1=$(printf ",%s" "${group1[@]}")
all_group1=${all_group1:1}

all_group2=$(printf ",%s" "${group2[@]}")
all_group2=${all_group2:1}

echo "benchmark, pearsons correlation, spearman rank" > ./$DEST/graphs/filtered.txt
echo "benchmark, unique methods" > ./$DEST/graphs/method-count.txt

if [[ $1 != "skip" ]]; then
  for benchmark in "${benchmarks[@]}"; do
    echo $benchmark 
    /usr/bin/python3.5 ./scripting/method-energy-proportion.py -path="./$DEST/${benchmark}" -benchmark=${benchmark} -destination="./$DEST/graphs/${benchmark}/" -kind="filtered"
    /usr/bin/python3.5 ./scripting/method-energy-proportion.py -path="./$DEST/${benchmark}" -benchmark=${benchmark} -destination="./$DEST/graphs/${benchmark}/" -kind="shallow"
    /usr/bin/python3.5 ./scripting/method-energy-proportion.py -path="./$DEST/${benchmark}" -benchmark=${benchmark} -destination="./$DEST/graphs/${benchmark}/" -kind="deep"
    /usr/bin/python3.5 ./scripting/cfa-method-energy-pie.py -path="./$DEST/${benchmark}" -benchmark=${benchmark} -destination="./$DEST/graphs/${benchmark}/"

    #/usr/bin/python3.5 ./scripting/thread-movement-benchmark.py -path="./$DEST/${benchmark}/" -benchmark=${benchmark} -destination="./$DEST/graphs/${benchmark}/"
  done
fi

#/usr/bin/python3.5 ./scripting/system-application-all-bench.py -path="./$DEST/" -benchmark=${all_group1} -destination="./$DEST/graphs/"
#/usr/bin/rsvg-convert -f pdf -o ./$DEST/graphs/application-attribution-group1.pdf ./$DEST/graphs/system-application-attribution-all-bench.svg 

/usr/bin/python3.5 ./scripting/system-application-all-bench.py -path="./$DEST/" -benchmark=${all_group2} -destination="./$DEST/graphs/"
/usr/bin/rsvg-convert -f pdf -o ./$DEST/graphs/application-attribution-group2.pdf ./$DEST/graphs/system-application-attribution-all-bench.svg 





