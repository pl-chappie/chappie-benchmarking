#!/bin/bash

# usage: Invoke with old to skip the method alignment, invoke with new to perform it:
#   ./analyze_dacapo.sh old
#   ./analyze_dacapo.sh new


benchmarks=(
avrora  
batik
eclipse 
fop
h2 
jython  
luindex 
lusearch 
pmd 
sunflow 
tomcat     
tradebeans 
tradesoap 
xalan
#ALSMatrixFactorization
#ConnectedComponents
#Pagerearnk
#twitter
#tpcc
#wikipedia
)

#indir=chappie.dacapo.VM_SAMPLE
#outdir=processed_data

indir=chappie.benchmark.VM_SAMPLE_FULL
outdir=fast_rate_data
mkdir $outdir

if [[ $# -ne 1 ]]; then
  echo "usage: $0 [CHAPPIE VERSION]"
  exit
fi

if [[ $1 = "new" ]]; then
  for benchmark in "${benchmarks[@]}"; do
    nlogs=`find ./$indir/${benchmark} -maxdepth 1 -type f -name '*.thread.*.csv' | wc -l`
    nlogs=$((nlogs-1))
    d=./$indir/${benchmark}
    echo $benchmark 
    #echo $benchmark > $d/aligned.csv

    for i in `seq 0 $nlogs`; do
      ./opt-align.rb -c $d/chappie.thread.$i.csv -h $d/log.$i.hpl > $d/aligned.$i.csv
      echo $i 
    done
    for i in `seq 0 $nlogs`; do
      mv $d/chappie.thread.$i.csv $d/saved.$i.csv
      mv $d/aligned.$i.csv $d/chappie.thread.$i.csv
    done
  done
fi

if [[ $1 = "check" ]]; then
  rm stats.txt
  touch stats.txt
  for benchmark in "${benchmarks[@]}"; do
    echo "================ $benchmark ================" >> stats.txt
    d=./$indir/${benchmark}
    echo $benchmark 
    ./check.rb -c $d >> stats.txt
  done
  exit
fi

for benchmark in "${benchmarks[@]}"; do
  echo $benchmark
  #Data Processing
  /usr/bin/python3.5 ./scripting/data_processing.py -path="./$indir/${benchmark}" -benchmark=${benchmark} -destination="$outdir/${benchmark}"

  /usr/bin/python3.5 ./scripting/data_processing_2.py -path="./$outdir/${benchmark}" -benchmark=${benchmark} -destination="$outdir/${benchmark}"

  #Abstraction Attribution script
  /usr/bin/python3.5 ./scripting/method-attribution.py -path="./$indir/${benchmark}" -benchmark=${benchmark} -destination="$outdir/${benchmark}"

  #Sanity
  #/usr/bin/python3.5 ./scripting/method-attribution-bak.py -path="./$indir/${benchmark}" -benchmark=${benchmark} -destination="$outdir/${benchmark}"

done


