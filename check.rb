#!/usr/bin/env ruby

require 'optparse'

RANGE=4

C_EPOCH=0
C_TIME=1
C_THREAD_NAME=2
C_TID=3
C_PID=4
C_CORE=5
C_UJIF=6
C_KJIF=7
C_STATE=8
C_STACK=9

def analyze(options)
  with_methods=0
  active_without_jiff=0
  active_with_jiff=0
  inactive=0


  for i in 0...10 do
    cf = File.open("#{options[:infile1]}/chappie.thread.#{i}.csv")
    cf.each_line do |line|
      next if line.include?("epoch")
      tok = line.split(",")
      tok[-1].chop!

      if tok[C_STACK] != "end" then
        with_methods += 1
        if tok[C_STATE] == "1" then
          if tok[C_UJIF] == "0" and tok[C_KJIF] == "0" then
            active_without_jiff += 1
          else
            active_with_jiff += 1
          end
        else
          inactive += 1
        end
      end
    end
  end

  puts "Thread samples with methods: #{with_methods}"
  puts "Active with jiffies: #{active_with_jiff}"
  puts "Active without jiffies: #{active_without_jiff}"
  puts "Inactive: #{inactive}"
  puts ""

  puts "Active with jiffies: #{(active_with_jiff.to_f/with_methods).to_f*100}%"
  puts "Active without jiffies: #{(active_without_jiff.to_f/with_methods.to_f)*100}%"
  puts "Inactive: #{(inactive.to_f/with_methods.to_f)*100}%"
  puts ""

end

if __FILE__ == $0
  options = {}
  options[:infile1] = ""

  optparse = OptionParser.new do |opts|
    opts.on("-c", "=MANDATORY", "chappie thread csv") do |o|
      options[:infile1] = o.to_s
    end
  end

  optparse.parse!

  analyze(options)

  
end 
