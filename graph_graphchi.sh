PATH="./processed_data_graphchi"
DEST="./processed_data_graphchi"

benchmarks=(
ConnectedComponents
Pagerank
)


for benchmark in "${benchmarks[@]}"; do
  echo "Graphing $benchmark ..."
  #Method Attribution script
  /usr/bin/python3.5 ./scripting/method-energy-proportion.py -path=$PATH/$benchmark -benchmark=$benchmark -destination=$DEST/${benchmark}
done
