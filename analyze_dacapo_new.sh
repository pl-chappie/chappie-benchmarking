#!/bin/bash

# usage: Invoke with old to skip the method alignment, invoke with new to perform it:
#   ./analyze_dacapo.sh old
#   ./analyze_dacapo.sh new


benchmarks=(
avrora 
batik
eclipse 
h2 
jython 
luindex 
lusearch 
pmd 
sunflow 
tomcat 
tradebeans 
tradesoap 
xalan
)


if [[ $# -ne 1 ]]; then
  echo "usage: $0 [CHAPPIE VERSION]"
  exit
fi

if [[ $1 = "new" ]]; then
  for benchmark in "${benchmarks[@]}"; do
    nlogs=`find ./chappie.dacapo/${benchmark} -maxdepth 1 -type f -name '*.thread.*.csv' | wc -l`
    nlogs=$((nlogs-1))
    d=./chappie.dacapo/${benchmark}
    for i in `seq 0 $nlogs`; do
      ./opt-align.rb -c $d/chappie.thread.$i.csv -h $d/log.$i.hpl > $d/aligned.$i.csv
      echo $i 
    done
    for i in `seq 0 $nlogs`; do
      mv $d/chappie.thread.$i.csv $d/saved.$i.csv
      mv $d/aligned.$i.csv $d/chappie.thread.$i.csv
    done
  done
fi

for benchmark in "${benchmarks[@]}"; do
  echo $benchmark
  #/home/rsaxena3/work/chappie/run/run.sh dacapo-9.12-bach.jar "" Harness "-no-validation ${benchmark}"
  #Data Processing
  /usr/bin/python3.5 ./scripting/data_processing.py -path="./chappie.dacapo/${benchmark}" -benchmark=${benchmark} -destination="processed_data/${benchmark}"
  /usr/bin/python3.5 ./scripting/data_processing_2.py -path="./chappie.dacapo/${benchmark}" -benchmark=${benchmark} -destination="processed_data/${benchmark}"
  #Method Attribution script
  /usr/bin/python3.5 ./scripting/method-attribution.py -path="./chappie.dacapo/${benchmark}" -benchmark=${benchmark} -destination="processed_data/${benchmark}"
  #in case of analyze, point to data processing and graph generaation scripts
done


