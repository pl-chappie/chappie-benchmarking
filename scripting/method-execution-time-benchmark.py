import pandas as pd
import argparse
import os
import re
import matplotlib as mpl
import matplotlib.pyplot as plt
if os.environ.get('DISPLAY','') == '':
    print('no display found. Using non-interactive Agg backend')
    mpl.use('Agg')

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-benchmark', action="store", default="sunflow", dest="benchmark")
parser.add_argument('-path', action="store", default="~/Desktop/sunflow", dest="path")
parser.add_argument('-destination', action="store", default="~/Desktop/sunflow/processed_data",
                    dest="destination")
args = parser.parse_args()

path = os.path.expanduser(args.path)
benchmark = args.benchmark
destination = os.path.expanduser(args.destination)
if not os.path.exists(destination):
    os.makedirs(destination)

numOfFiles = 0
for entry in os.scandir(path):
    if entry.is_file() and "chappie.thread" in entry.name:
        print(entry.name)
        numOfFiles += 1

for i in range(1):
    method = pd.read_csv(path + '/chappie.thread.'+str(i)+'.csv')
    result = pd.read_csv(destination + '/result_'+str(i)+'.csv')
    method_energy = pd.DataFrame(columns=['epoch', 'method', 'dram1', 'package1', 'dram2', 'package2', 'energy'])

    #method = method.loc[method['core'] != -1]
    method = method.loc[method['thread'] != "Chaperone"]

    # filter inactive threads
    method = method.loc[method['state'] != False]

    #result = result.loc[result['core'] != -1]
    #result = result.head(20)
    epochs = list(result['epoch'].unique())
    print(epochs)

    #method_count = pd.DataFrame(columns=['method', 'counter'])
    count = 0
    #epochs = list(range(300))
    method_counter = {}
    for epoch in epochs:
        print('Processing Epoch: ' + str(epoch))
        methods = list((method.loc[(method['epoch'] == epoch)])['stack'])
        for m in methods:
            stack = m.split(';')
            first_method = stack[0]
            if first_method == "end":
                continue
            if "java." in m:
                continue

            first_method = re.sub(r"\(+.*\)+", "", first_method)
            temp = first_method.split(".")
            temp = temp[len(temp) - 2: len(temp)]
            new_method_name = str(temp[0] + '.' + temp[1])

            if new_method_name in method_counter:
                method_counter[new_method_name] +=1
            else:
                method_counter[new_method_name] = 1
            #method_count.loc[count] = [new_method_name, 1]
            count += 1

    #method_count = method_count.groupby(['method'])['counter'].sum()
    #print(method_count)
    print(method_counter)
    counter = sorted(method_counter.items())
    counter = sorted(counter, key=lambda t: t[1], reverse=True)[:10]
    print(counter)
    x, y = zip(*counter)
    plt.figure(figsize=(15,10))
    ax = plt.bar(x, y, color='green')
    plt.ylabel('Number of method hits')
    plt.xticks(rotation=90)
    plt.tight_layout()
    plt.show()
    plt.close()