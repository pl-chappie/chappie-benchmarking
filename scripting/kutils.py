import numpy as np

def normalize(list):
    standard_deviation = np.std(list)
    average = np.mean(list)
    list = (list - standard_deviation)/average
    print list

def write_file(path, list):
    f = open(path, 'w')
    for line in list:
        f.write(line)
        f.write("\n")



def read_csv_as_array(path):
    temp_array=[]
    with open(path) as f:
        for line in f:           
            fields=line.split(",")
            temp_array.append(fields)
    return temp_array
