import pandas as pd
import os
import argparse

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-reference', action="store", dest="reference")
parser.add_argument('-coapp', action="store", dest="coapp")
parser.add_argument('-resultfile', action="store", dest="result")
parser.add_argument('-tag', action="store", dest="tag")
args = parser.parse_args()

refData = pd.read_csv(args.reference)
coappData = pd.read_csv(args.coapp)

refData = refData.drop(columns=['Energy','Time'])
coappData = coappData.drop(columns=['Energy','Time'])
refData = refData.rename(columns={'RawEnergy': 'RefEnergy'})
coappData = coappData.rename(columns={'RawEnergy':'CoAppEnergy'})

print(refData.head(10))
print(coappData.head(10))

joinedData = refData.set_index('method').join(coappData.set_index('method')).fillna(0)
print(joinedData)

niceformat = args.tag.replace(" ", "").replace(",","-")

joinedData.to_csv('./scripting/{}_ranking.csv'.format(niceformat), index=True)

pearson = joinedData.corr(method='pearson').iloc[0]['CoAppEnergy'] 
spearman = joinedData.corr(method='spearman').iloc[0]['CoAppEnergy']


f = open(args.result, 'a')
f.write('{},  {},  {}\n'.format(args.tag,round(pearson,2),round(spearman,2)))
