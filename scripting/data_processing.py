#!/usr/bin/python3

import sys

import os
import argparse
import numpy as np
import pandas as pd

rapl_wrap_around = 16384

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-benchmark', action="store", default="graphchi", dest="benchmark")
parser.add_argument('-path', action="store", default="./chappie.chappie_test", dest="path")
parser.add_argument('-destination', action="store", default="./chappie.chappie_test/processed", dest="destination")
args = parser.parse_args()

path = os.path.expanduser(args.path)
benchmark = args.benchmark
destination = os.path.expanduser(args.destination)
if not os.path.exists(destination):
    os.makedirs(destination)

#path = './chappie.chappie_test.1'
#destination = './chappie.chappie_test.1.processed/'

traces = np.sort([f for f in os.listdir(path) if 'trace' in f])
threads = np.sort([f for f in os.listdir(path) if 'thread' in f])
jiffies = np.sort([f for f in os.listdir(path) if 'jiffies' in f and 'clean' not in f])
apps = np.sort([f for f in os.listdir(path) if 'application' in f and 'clean' not in f])

if len(jiffies) > 0:
    jiffy_dfs = []

    for app, jiffy in zip(apps, jiffies):
        print(jiffy)
        jif = pd.read_csv(os.path.join(path, jiffy), header = None)

        # separate each line into the desired pieces
        jif = jif[jif[0].str.contains('cpu\d+')]
        jif = jif[0].str.replace('  ', ' ').str.split(' ').map(lambda x: [x[0]] + [int(x[i]) for i in range(1, len(x))]).values.tolist()
        jif = pd.DataFrame(jif, columns = ['core'] + ['jiffies{}'.format(i) for i in range(10)], dtype = int)

        # add the chappie values
        jif['core'] = jif['core'].map(lambda x: 'cpu40' if x == 'cpu' else x)
        jif['core'] = jif['core'].str.replace("cpu", '0').astype(int)
        jif['epoch'] = [n for N in [[i] * 40 for i in range(len(jif)//40)] for n in N]
        jif['socket'] = jif['core'].map(lambda c: 0 if c == 40 else 2 if c >= 20 else 1)

        jif = pd.merge(jif, jif.groupby('core').min(), on = 'core', suffixes = ('', '_'))
        for i in range(10):
            jif['jiffies{}'.format(i)] -= jif['jiffies{}_'.format(i)]

        jif = jif.groupby(['epoch', 'socket']).sum().reset_index()
        jif = jif[jif['epoch'] % 10 == 0]

        epoch = jif['epoch']
        socket = jif['socket']
        jif = jif.groupby('socket').diff().fillna(0)
        jif['epoch'] = epoch
        jif['socket'] = socket
        jif = jif.drop(columns = [col for col in jif.columns if '_' in col])
        jif['jiffies'] = sum(jif['jiffies{}'.format(i)] for i in range(10) if i != 3)

        appl = pd.read_csv(os.path.join(path, app))
        jiffies_factor = 10
        appl = appl[appl['epoch'] % jiffies_factor == 0]

        appl = pd.merge(appl, appl.groupby('tid').min(), on = 'tid', suffixes = ('', '_'))
        appl['u_jiffies'] = appl['u_jiffies'] - appl['u_jiffies_']
        appl['k_jiffies'] = appl['k_jiffies'] - appl['k_jiffies_']

        epoch = appl['epoch']
        core = appl['core']
        tid = appl['tid']
        appl = appl.groupby('tid').diff().fillna(0)
        appl['epoch'] = epoch
        appl['core'] = core
        appl['tid'] = tid

        appl['socket'] = appl['core'].map(lambda c: 0 if c == 40 else 2 if c >= 20 else 1)
        appl = appl.groupby(['epoch', 'socket']).sum().reset_index()

        jif = pd.merge(jif, appl, on = ['epoch', 'socket'])
        jif['os_state'] = (jif['u_jiffies'] + jif['k_jiffies']) / (jif['jiffies'])
        jif['os_state'] = jif['os_state'].replace(np.inf, 1)
        jif['os_state'] = jif['os_state'].map(lambda x: min(x, 1))
        jif = jif[['epoch', 'socket', 'jiffies', 'os_state']].fillna(0)

        jiffy_dfs.append(jif)
else:
    jiffy_dfs = [None] * len(traces)

energy_stats = pd.DataFrame()
frac = []

for i, (thread, trace, jiffy) in enumerate(zip(threads, traces, jiffy_dfs)):
    print('{}, {}'.format(thread, trace))
    trace = pd.read_csv(os.path.join(path, trace))

    trace = trace[['epoch','socket', 'package', 'dram']]
    epoch = trace['epoch']
    socket = trace['socket']

    df = pd.merge(trace, trace.groupby('socket').min(), on = 'socket')
    df['epoch'] = df['epoch_x']
    df['package'] = df['package_x'] - df['package_y']
    df['dram'] = df['dram_x'] - df['dram_y']
    trace = df[['epoch', 'socket', 'package', 'dram']]

    epoch = trace['epoch']
    socket = trace['socket']
    trace = trace.groupby('socket').diff().fillna(0)
    trace['epoch'] = epoch
    trace['socket'] = socket
    trace['package'] = trace['package'].map(lambda x: max(x + rapl_wrap_around, 0) if x < 0 else x)
    trace['dram'] = trace['dram'].map(lambda x: max(x + rapl_wrap_around, 0) if x < 0 else x)

    trace.to_csv(os.path.join(destination, 'chappie.trace.{}.csv'.format(i)), index = False)

    print(os.path.join(path, thread))

    thread = pd.read_csv(os.path.join(path, thread))

    thread['socket'] = thread['core'].transform(lambda x: 2 if x > 19 else 1 if x > -1 else -1)

    unmappables = thread[thread['socket'] == -1].copy()
    unmappables['state'] /= 2

    unmappables['socket'] = 1

    unmappables2 = unmappables.copy()
    unmappables2['socket'] = 2

    thread = pd.concat([thread[thread['socket'] != -1], unmappables, unmappables2]).sort_values(by = 'epoch')

    thread = pd.merge(thread, jiffy, on = ['epoch', 'socket'], how = 'left')

    threads = thread['thread']
    thread = thread.groupby('thread').fillna(method = 'ffill').reset_index().fillna(0)
    thread['thread'] = threads
    frac.append(thread['os_state'].mean())

    jrapl_factor = 2
    thread['d_epoch'] = thread['epoch'] // jrapl_factor

    activity = thread.groupby(['d_epoch', 'socket'])['state'].sum().reset_index().rename(columns = {'state': 'count'})
    thread = pd.merge(thread, activity, on = ['d_epoch', 'socket'])
    thread['vm_state'] = thread['state'] / thread['count']

    trace['d_epoch'] = trace['epoch'] // jrapl_factor
    thread = pd.merge(thread, trace, on = ['d_epoch', 'socket']).fillna(0)
    thread['epoch'] = thread['epoch_x']

    thread['package'] = thread['package'] * thread['os_state'] * thread['vm_state']
    thread['dram'] = thread['dram'] * thread['os_state'] * thread['vm_state']

    thread['package'] = thread['package'].fillna(0)
    thread['dram'] = thread['dram'].fillna(0)

    energy = pd.DataFrame({
        'TOTAL': [trace['package'].sum()],
        'ALL THREADS': [thread['package'].sum()],
        # 'MAPPED THREADS': [thread['package2'].sum()],
    })
    print(energy)
    energy_stats = energy_stats.append(energy)

    thread[thread['core'] > -1][['epoch', 'thread', 'core', 'socket', 'package', 'dram', 'stack']].to_csv('{}/result_{}.csv'.format(destination, i), index = False)

    thread['active'] = thread['vm_state'] > 0
    thread[['epoch', 'thread', 'core', 'socket', 'package', 'dram', 'active', 'stack']].to_csv('{}/component_{}.csv'.format(destination, i), index = False)

print('Jiffies contribution: {:2f} %'.format(np.mean(frac) * 100))
energy_stats = energy_stats.mean()
energy_stats['BENCHMARK'] = benchmark
print(energy_stats)
energy_stats.to_csv('energy_stats.csv', mode = 'a+')
