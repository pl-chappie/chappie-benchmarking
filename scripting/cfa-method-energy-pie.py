import pandas as pd
import os
import argparse
import re
import matplotlib as mpl

if os.environ.get('DISPLAY', '') == '':
    mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages

method_kinds = ['filtered','shallow','deep','library']

def adjustlabel(x):
    methods = x.split("|")
    methods.pop(0)
    lbl = "["
    for i,m in enumerate(methods):
        lbl += m
        if i+1 < len(methods):
            lbl += ","
    lbl += "]"
    return lbl

def adjustlabel2(x):
    methods = x.split("|")
    methods.pop(0)
    lbl = "["
    for i,m in enumerate(methods):
        lbl += m.split(".")[1]
        if i+1 < len(methods):
            lbl += ","
    lbl += "]"
    return lbl

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-benchmark', action="store", default="sunflow", dest="benchmark")
parser.add_argument('-path', action="store", default="~/Desktop/dacapo/sunflow/", dest="path")
parser.add_argument('-destination', action="store", default="~/Desktop/dacapo/sunflow/test/", dest="destination")
args = parser.parse_args()

path = os.path.expanduser(args.path)
benchmark = args.benchmark
destination = os.path.expanduser(args.destination)
if not os.path.exists(destination):
    os.makedirs(destination)


for k in method_kinds:
    total_energy = 0

    bigData = pd.DataFrame(columns=['method', 'dram1', 'package1', 'dram2', 'package2', 'energy'])
    cfData = pd.DataFrame(columns=['method', 'dram1', 'package1', 'dram2', 'package2', 'energy'])

    total_energy = 0

    numOfFiles = 0
    for entry in os.scandir(path):
        if entry.is_file() and "{}_attribution".format(k) in entry.name:
            numOfFiles += 1

    for i in range(3, numOfFiles):
        df = pd.read_csv(path + '/{}_attribution_'.format(k) + str(i) + '.csv')
        cfData = cfData.append(df, ignore_index=True)
        cfData = cfData.fillna(value=0)

    #cfData['method'] = cfData['method'].apply(lambda x: re.sub(r"\(+.*\)+", "", x))
    #cfData['method'] = cfData['method'].apply(lambda x: ((x.split("."))[len(x.split("."))-2]) + '.'+ ((x.split("."))[len(x.split("."))-1]))

    cfData = cfData.groupby(['method'])['dram1', 'package1', 'dram2', 'package2', 'energy'] \
        .sum(numeric_only=False)
    cfData.reset_index(inplace=True)
    cfData = cfData.sort_values("energy",ascending=False).head(3)
    cfData.to_csv('{}/{}_top_methods.csv'.format(destination,k), index=False)

    numOfFiles = 0
    for entry in os.scandir(path):
        if entry.is_file() and "{}_2cfa".format(k) in entry.name:
            numOfFiles += 1
    for i in range(3, numOfFiles):
        df = pd.read_csv(path + '/{}_2cfa_'.format(k) + str(i) + '.csv')
        bigData = bigData.append(df, ignore_index=True)
        bigData = bigData.fillna(value=0)

    bigData = bigData.groupby(['method'])['dram1', 'package1', 'dram2', 'package2', 'energy'] \
        .sum(numeric_only=False)
    bigData.reset_index(inplace=True)
    bigData.to_csv('{}/{}_top_methods_cfa2.csv'.format(destination,k), index=False)

    mpl.rcParams.update({'font.size': 10})

    topAnalysis = np.array(cfData['method'])
    cnt = 0
    for md in topAnalysis:
        with PdfPages(destination + '/{}-2cfa-method-energy-pie-'.format(k) + benchmark + '-' + str(cnt) + '.pdf') as pdf:
            print(md)
            callingcontexts = bigData[bigData['method'].str.startswith(md)]
            callingcontexts['method'] = callingcontexts['method'].map(lambda x: adjustlabel2(x))
            totalcallingenergy = callingcontexts['energy'].sum()
            callingcontexts['energy'] = callingcontexts['energy'] / totalcallingenergy
            print(callingcontexts)
            ax = callingcontexts.plot(title=md, kind='pie', labels=None, y=['energy'],figsize=(4, 4))
            ax.legend(labels=callingcontexts['method'],prop={'size':9},loc=2)
            ax.get_yaxis().set_visible(False)


            #plt.ylabel('Percent Consumption of Energy',fontsize=10)

            #plt.xlabel('Percent Consumption of Energy')
            plt.tight_layout()
            pdf.savefig()
            #plt.show()
            plt.close()
            cnt += 1
