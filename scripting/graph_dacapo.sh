#!/bin/bash

PATH="./processed_data"
DEST="processed_data"


benchmarks=(
eclipse 
)

all_benchmarks=$(printf ",%s" "${benchmarks[@]}")
all_benchmarks=${all_benchmarks:1}

echo "benchmark, pearsons correlation, spearman rank" > ./processed_data/graphs/filtered.txt
if [[ $1 != "skip" ]]; then
  for benchmark in "${benchmarks[@]}"; do
    echo $benchmark 
    /usr/bin/python3.5 method-energy-proportion.py -path="./processed_data/${benchmark}" -benchmark=${benchmark} -destination="./processed_data/graphs/${benchmark}/" -kind="filtered"
    /usr/bin/python3.5 method-energy-proportion.py -path="./processed_data/${benchmark}" -benchmark=${benchmark} -destination="./processed_data/graphs/${benchmark}/" -kind="shallow"
    /usr/bin/python3.5 .method-energy-proportion.py -path="./processed_data/${benchmark}" -benchmark=${benchmark} -destination="./processed_data/graphs/${benchmark}/" -kind="deep"
    /usr/bin/python3.5 cfa-method-energy-pie.py -path="./processed_data/${benchmark}" -benchmark=${benchmark} -destination="./processed_data/graphs/${benchmark}/"

#    /usr/bin/python3.5 ./scripting/thread-movement-benchmark.py -path="./processed_data/${benchmark}/" -benchmark=${benchmark} -destination="./processed_data/graphs/${benchmark}/"
  done
fi

/usr/bin/python3.5 system-application-all-bench.py -path="./processed_data/" -benchmark=${all_benchmarks} -destination="./processed_data/graphs/"
/usr/bin/rsvg-convert -f pdf -o ./processed_data/graphs/system-application-attribution-all-bench.pdf ./processed_data/graphs/system-application-attribution-all-bench.svg 




