#!/bin/bash

rm -rf chappie.dacapo
mkdir chappie.dacapo

export MODE=3
export ITERS=10

benchmarks=(
batik
sunflow 
avrora 
eclipse 
h2 
jython 
luindex 
lusearch 
#pmd 
#tomcat 
#tradebeans 
#tradesoap 
xalan
)

for benchmark in "${benchmarks[@]}"; do
  echo $benchmark
  /home/rsaxena3/work/chappie/run/run.sh dacapo-9.12-bach.jar "" Harness "-no-validation ${benchmark}"
  #in case of analyze, point to data processing and graph generaation scripts
  mkdir $benchmark
  mv chappie.dacapo-9.12-bach/*.csv $benchmark
  mv $benchmark chappie.dacapo/$benchmark
done
