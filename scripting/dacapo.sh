#!/bin/bash

#rm -rf chappie.dacapo
#mkdir chappie.dacapo

export MODE=VM_SAMPLE
export ITERS=10
export STACK_PRINT=true
export MEMORY=true
export POLLING=4

benchmarks=(
avrora 
#batik
#eclipse 
#h2 
#jython 
#luindex 
#lusearch 
#pmd 
#sunflow 
#tomcat 
#tradebeans 
#  tradesoap 
#xalan
)

for benchmark in "${benchmarks[@]}"; do
  echo $benchmark
  /home/acanino1/Projects/chappie/run/run.sh dacapo-9.12-bach.jar "" Harness "-no-validation ${benchmark}"
  mkdir $benchmark
  mv chappie.dacapo-9.12-bach/*.csv $benchmark
  mv chappie.dacapo-9.12-bach/*.hpl $benchmark
  mv $benchmark chappie.dacapo/
done
