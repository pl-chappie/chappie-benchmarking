import os
import argparse
import math
import pandas as pd
import numpy as np
import matplotlib as mpl
if os.environ.get('DISPLAY','') == '':
    print('no display found. Using non-interactive Agg backend')
    mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-benchmark', action="store", default="tpcc", dest="benchmark")
parser.add_argument('-path', action="store", default="~/Desktop/tpcc/", dest="path")
parser.add_argument('-destination', action="store", default="~/Desktop/tpcc/test/", dest="destination")
args = parser.parse_args()


path = os.path.expanduser(args.path)
benchmark = args.benchmark
destination = os.path.expanduser(args.destination)

if not os.path.exists(destination):
    os.makedirs(destination)

numOfFiles = 0
for entry in os.scandir(path):
    if entry.is_file() and "result" in entry.name:
        numOfFiles += 1

for i in range(numOfFiles):
    with PdfPages('{}/application-attribution-{}_{}.pdf'.format(destination, benchmark, i)) as pdf:
        print('application-attribution-{}_{}.pdf'.format(benchmark, i))
        df = pd.read_csv(path + '/result_'+str(i)+'.csv')
        df = df.loc[~df['thread'].str.startswith(('Reference Handler', 'Chaperone', 'main', 'Signal Dispatcher', 'Finalizer'))]
        df = df.loc[df['core'] != -1]


        #Check to see if dataframe is empty. Since it can't process further, continue
        if(df.empty):
            print("Empty DataFrame after removing system threads. Can't plot graph")
            continue
        df['energy'] = df['package'] + df['dram']
        df = df.groupby(['epoch', 'thread'], as_index=False, sort=False)['energy'].sum()
        df = df.pivot(index='epoch', columns='thread', values='energy')
        threads = list(df.columns)

        bins = math.ceil(len(df) / 40)
        columns = list(df.columns)
        columns.insert(0, 'epoch')
        df.reset_index(inplace=True)
        df.columns = columns

        if len(df) < bins:
            split = np.array_split(df, 1)
        else:
            split = np.array_split(df, len(df)/bins)

        newDf = pd.DataFrame(columns=columns)


        for frame in range(len(split)):
            epochInd = split[frame]['epoch'].max()
            newDf.loc[frame] = split[frame].sum()
            newDf.loc[frame]['epoch'] = epochInd

        #newDf['epoch'] = newDf['epoch'].astype(int)
        newDf.set_index('epoch', inplace=True)
        ax = newDf.plot.bar(stacked=True, figsize=(25, 15))

        plt.legend(loc='best', fontsize=20)
        plt.xlabel('Elapsed Time ({} ms as 1 Unit)'.format(bins * 4), fontsize=25)
        plt.ylabel('Energy (J)', fontsize=25)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)

        pdf.savefig()
        plt.close()

