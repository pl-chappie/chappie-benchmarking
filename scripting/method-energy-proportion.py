import pandas as pd
import os
import argparse
import matplotlib as mpl

if os.environ.get('DISPLAY', '') == '':
    mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-benchmark', action="store", default="sunflow", dest="benchmark")
parser.add_argument('-path', action="store", default="~/Desktop/dacapo/sunflow/", dest="path")
parser.add_argument('-destination', action="store", default="~/Desktop/dacapo/sunflow/test/",
                    dest="destination")
parser.add_argument('-kind', action="store", default="java", dest="kind")
parser.add_argument('-skip', action="store", default="false", dest="skip")
args = parser.parse_args()

path = os.path.expanduser(args.path)
benchmark = args.benchmark
kind = args.kind
destination = os.path.expanduser(args.destination)
should_skip = args.skip

fileName = ''
outputName = ''

if not os.path.exists(destination):
    os.makedirs(destination)

if kind in 'filtered':
    fileName = 'filtered'
    outputName = 'all'

elif kind in 'shallow':
    fileName = 'shallow'
    outputName = 'unfiltered'

elif kind in 'deep':
    fileName = 'deep'
    outputName = 'deep'

elif kind in 'library':
    fileName = 'library'
    outputName = 'library'

else:
    print("Incorrect method kind!")
    exit()

hardcoded='./processed_data/graphs/method-count.txt'

abstractions = ['attribution', 'class', 'package']
for absn in abstractions:
    energyData = pd.DataFrame(columns=['method', 'dram1', 'package1', 'dram2', 'package2', 'energy'])
    total_energy = 0

    hitData = pd.DataFrame(columns=['method', 'hits'])
    total_hits = 0

    kindAbsnFile = "{}_{}".format(kind,absn)
    kindHotnessFile = "{}_hotness".format(kind,absn)

    numOfFiles = 0
    for entry in os.scandir(path):
        if entry.is_file() and kindAbsnFile in entry.name:
            numOfFiles += 1

    for i in range(0, numOfFiles):
        df = pd.read_csv('{}/{}_{}_{}.csv'.format(path, fileName, absn, i))
        energyData = energyData.append(df, ignore_index=True)
        energyData = energyData.fillna(value=0)

        data = pd.read_csv('{}/shallow_{}_{}.csv'.format(path, absn, i))

        energy = data['energy'].sum()
        total_energy += energy

        # If we process for attribution (method), process hits
        if absn == 'attribution':
            df = pd.read_csv('{}/{}_hotness_{}.csv'.format(path, fileName, i))
            hitData = hitData.append(df, ignore_index=True)
            hitData = hitData.fillna(value=0)

            data = pd.read_csv('{}/shallow_hotness_{}.csv'.format(path, i))
            hits = data['hits'].sum()
            total_hits += hits



    energyData = energyData.groupby(['method'])['dram1', 'package1', 'dram2', 'package2', 'energy'] \
        .sum(numeric_only=False)
    energyData.reset_index(inplace=True)

    if absn == 'attribution':
        hitData = hitData.groupby(['method'])['hits'].sum().reset_index()

    with PdfPages('{}/{}-{}-energy-proportion-{}.pdf'.format(destination, outputName, absn,benchmark)) as pdf:
        if absn == 'attribution':
            newDf = pd.DataFrame(columns=['method', 'Energy', 'Time','RawEnergy'])
            crln = pd.DataFrame(columns=['method', 'Energy', 'Time'])
        else:
            newDf = pd.DataFrame(columns=['method', 'Energy'])

        count = 0
        for method in energyData['method']:
            energy = float(energyData.loc[energyData['method'] == method]['energy'])
            percent_energy = energy / total_energy * 100

            if absn == 'attribution':
                hits = float(hitData.loc[hitData['method'] == method]['hits'])
                percent_hits = hits / total_hits * 100
                newDf.loc[count] = [method, percent_energy, percent_hits, energy]
                crln.loc[count] = [method,energy,hits]
            else:
                newDf.loc[count] = [method, percent_energy]

            count += 1

        newDf = newDf.loc[newDf['method'] != 'Thread.getStackTrace']
        newDf = newDf.sort_values('Energy',ascending=False)

        print(newDf.head(10))

        if absn == 'attribution':
            # Playing around with correlation
            pearson = crln.corr(method='pearson').iloc[0]['Time'] 
            spearman = crln.corr(method='spearman').iloc[0]['Time']


            df = pd.read_csv('{}/{}_{}_{}.csv'.format(path, fileName, absn, i))


            if not should_skip:
                f = open('./processed_data/graphs/' + kind + '.txt', 'a')
                f.write('{}, {}, {}\n'.format(benchmark,pearson,spearman))

        if absn == 'attribution' and kind == 'shallow':
            count = len(newDf['method'].unique())
            f = open(hardcoded, 'a')
            f.write('{}, {}\n'.format(benchmark,count))


        # remove Thread.getStackTrace
        newDf.to_csv('{}/{}_{}_ranking.csv'.format(destination, kind ,absn), index=False)
        newDf = newDf.head(10)
        #print(newDf)

        mpl.rcParams.update({'font.size': 11})

        x = np.array(newDf['method'])

        ax = None
        if absn == 'attribution':
            ax = newDf.plot(kind='barh', x='method', y=['Energy','Time'], width=0.3, figsize=(6, 3.5))
        else:
            ax = newDf.plot(kind='barh', x='method', y=['Energy'], width=0.3, figsize=(6, 3.5), color='tab:blue')
            ax.get_legend().remove()

        ax.set_yticklabels([])
        ax.invert_yaxis()
        ax.tick_params(
            axis='y',  # changes apply to the x-axis
            which='both',  # both major and minor ticks are affected
            bottom=False,  # ticks along the bottom edge are off
            top=False,  # ticks along the top edge are off
            labelbottom=False)  # labels along the bottom edge are off
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)

        for i, v in enumerate(x):
            ax.text(0, i - .20, str(v), color='black')

        plt.ylabel('',fontsize=12)
        plt.xlabel('',fontsize=12)


        plt.tight_layout()
        pdf.savefig()
        #plt.show()
        plt.close()
