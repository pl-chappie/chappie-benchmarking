import os
import argparse
import pandas as pd
import numpy as np
import matplotlib as mpl
import math

if os.environ.get('DISPLAY','') == '':
    print('no display found. Using non-interactive Agg backend')
    mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-benchmark', action="store", default="avrora", dest="benchmark")
parser.add_argument('-path', action="store", default="~/Desktop/processed_data_dacapo/avrora/", dest="path")
parser.add_argument('-destination', action="store", default="~/Desktop/processed_data_dacapo/graphs/h2/", dest="destination")
args = parser.parse_args()

path = os.path.expanduser(args.path)
benchmark = args.benchmark
destination = os.path.expanduser(args.destination)
if not os.path.exists(destination):
    os.makedirs(destination)

numOfFiles = 0
for entry in os.scandir(path):
    if entry.is_file() and "system-application_" in entry.name:
        numOfFiles += 1

for i in range(numOfFiles):
    with PdfPages(destination+'/system-application-attribution-'+benchmark+ '-' + str(i) + '.pdf') as pdf:
        df = pd.read_csv(path+'/system-application_'+str(i)+'.csv')
        bins = math.ceil(len(df)/25)
        print('Bin size: {}'.format(math.ceil(bins)))
        if len(df) < bins:
            split = np.array_split(df, 1)
        else:
            split = np.array_split(df, len(df)/bins)
        newDf = pd.DataFrame(columns=['time', 'package_system', 'dram_system', 'package_sock1', 'package_sock2', \
                                      'dram_sock1', 'dram_sock2'])
        for frame in range(len(split)):
            ind = split[frame].index.max()
            time = split[frame].loc[ind]['time']
            package_system = split[frame]['sys_package'].sum()
            dram_system = split[frame]['sys_dram'].sum()

            package_sock1 = split[frame]['sock1_app_package'].sum()
            package_sock2 = split[frame]['sock2_app_package'].sum()
            dram_sock1 = split[frame]['sock1_app_dram'].sum()
            dram_sock2 = split[frame]['sock2_app_dram'].sum()
            newDf.loc[frame] = [time, package_system, dram_system, package_sock1, package_sock2, dram_sock1, dram_sock2]

        #newDf['time']= newDf['time'].astype(str)
        newDf.set_index('time', inplace=True)
        plot_params = ['package_system', 'dram_system', 'package_sock1', 'package_sock2', 'dram_sock1', 'dram_sock2']
        ax = newDf.plot.bar(stacked=True, figsize=(12, 6))

        plt.legend(loc='best')
        plt.xlabel('Cumulative energy of {} epochs in one bar'.format(bins), fontsize=12)
        plt.ylabel('Energy (J)', fontsize=12)
        plt.xticks(fontsize=8)
        plt.yticks(fontsize=8)

        pdf.savefig()
        plt.close()
