#!/usr/bin/python3

import os
import argparse
import numpy as np
import pandas as pd

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-benchmark', action="store", default="graphchi", dest="benchmark")
parser.add_argument('-path', action="store", default="./chappie.chappie_test", dest="path")
parser.add_argument('-destination', action="store", default="./chappie.chappie_test/processed", dest="destination")
args = parser.parse_args()

path = os.path.expanduser(args.path)
benchmark = args.benchmark
destination = os.path.expanduser(args.destination)
if not os.path.exists(destination):
    os.makedirs(destination)

components = np.sort([f for f in os.listdir(path) if 'component' in f])

for i, com in enumerate(components):
   print(os.path.join(path, com))
   attributedData = pd.read_csv(os.path.join(path, com))

   attributedData = attributedData[['epoch','core','socket', 'package', 'dram','active']]
   #epoch = attributedData['epoch']
   #socket = attributedData['socket']

   #trace['epoch'] = epoch
   #trace['socket'] = socket

   attributedData = attributedData[attributedData['active']]
   system_stamps  = attributedData[attributedData['core'] == -1]
   app_stamps     = attributedData[attributedData['core'] != -1]
   
   
   #.groupby('epoch').sum()

   # Going for system package
   system_stamps = system_stamps.groupby('epoch').sum()

   # Going for application package

   # Force grouping to work the way I want it to, don't know the right way to do this
   app_stamps = app_stamps.append({'epoch':-1, 'core':0, 'socket':1, 'package':0.0, 'dram':0.0, 'active':True}, ignore_index=True)
   app_stamps = app_stamps.append({'epoch':-1, 'core':39, 'socket':2, 'package':0.0, 'dram':0.0, 'active':True}, ignore_index=True)


   app_stamps = app_stamps.groupby(['epoch','socket'])[['package','dram']].sum().reset_index()

   system_stamps = system_stamps.rename(columns={'package': 'sys_package', 'dram': 'sys_dram'})

   app_stamps = app_stamps.pivot(index = 'epoch', values = ['package', 'dram'], columns = 'socket')
   app_stamps.columns = app_stamps.columns.droplevel()
   app_stamps = app_stamps.reset_index().fillna(0)
   app_stamps.columns = ['epoch', 'sock1_app_package', 'sock2_app_package', 'sock1_app_dram', 'sock2_app_dram']

   #print(app_stamps)

   #socket = pd.merge(thread.groupby(['epoch', 'socket']).sum(), trace, on = ['epoch', 'socket'])
   #socket = socket.pivot(index = 'epoch', values = ['package', 'dram'], columns = 'socket')
   #socket.columns = socket.columns.droplevel()
   #socket = socket.reset_index().fillna(0)
   #socket.columns = ['epoch', 'sock1_app_package', 'sock2_app_package', 'sock1_app_dram', 'sock2_app_dram']

   all_stamps = pd.merge(system_stamps, app_stamps, on = 'epoch')[['epoch', 'sys_package', 'sys_dram', 'sock1_app_package', 'sock1_app_dram', 'sock2_app_package', 'sock2_app_dram']]
   all_stamps = all_stamps.rename(columns = {'epoch': 'time'})

   all_stamps.to_csv('{}/system-application_{}.csv'.format(destination, i), index = False)
