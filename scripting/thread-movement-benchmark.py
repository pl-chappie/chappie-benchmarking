import os
import argparse
import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

mpl.use('Agg')

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-benchmark', action="store", default="graphchi", dest="benchmark")
parser.add_argument('-path', action="store", default="~/Desktop/", dest="path")
parser.add_argument('-destination', action="store", default="./benchmark_graphs", dest="destination")
args = parser.parse_args()

path = args.path
benchmark = args.benchmark
destination = args.destination
#path = '~/Desktop/'
#benchmark = 'graphchi'
if not os.path.exists(destination):
    os.makedirs(destination)

numOfFiles = 0
for entry in os.scandir(path):
    if entry.is_file() and "result" in entry.name:
        numOfFiles += 1

for i in range(numOfFiles):
    with PdfPages(destination+'/thread-movement-' + benchmark + '-' + str(i) + '.pdf') as pdf:
        df = pd.read_csv(path + '/result_'+str(i)+'.csv')
        for choice in ['Reference Handler', 'Chaperone', 'main', 'Signal Dispatcher', 'Finalizer']:
            df = df[df['thread'] != choice]
        if df.empty:
          print('Dataframe is empty after removing system threads in result_{}.csv. Unable to plot'.format(i))
          continue
        threads = list(df['thread'].unique()[:5])
        df = df[df['thread'].isin(threads)]
        df_pivot = df.pivot_table(index='epoch', columns='thread', values='core')
        df_pivot.fillna(value=0, inplace=True)

        x = np.array(df_pivot.index)
        #df_pivot.to_csv('~/Desktop/thread-movement.csv')

        plotList = []
        labels = []
        for p in range(len(df_pivot.columns)):
          plotList.append(tuple(np.array(df_pivot.iloc[:, p])))
          labels.append(df_pivot.columns[p]) 

        colors = ['skyblue', 'green', 'red', 'violet', 'orange']
        mkrs = ['.', '^', 's', 'p', '*']
        index = 0
        plt.figure(figsize=(6.5, 3.5))
        for plotTuple in plotList:
          plt.plot(x, plotTuple, data=df, marker=mkrs[index],markevery=50,markerfacecolor=colors[index], markersize=3, color=colors[index], linewidth=1)
          index+=1

        ax = plt.gca()
        plt.yticks(np.arange(0, 40, 1), fontsize=10)
        plt.xticks(fontsize=8)
        plt.xlabel('Elapsed Time (4ms as 1 unit)', fontsize=13)
        plt.ylabel('Thread Location (40 Cores)', fontsize=13)
        for label in ax.yaxis.get_ticklabels()[::2]:
            label.set_visible(False)

        #plt.legend(labels, prop={'size': 12})
        plt.tight_layout()
        pdf.savefig()
        plt.show()
        plt.close()
