import numpy as np
import argparse
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import kutils
from matplotlib.backends.backend_pdf import PdfPages

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-loadtype', action="store", default=".", dest="loadtype")
args = parser.parse_args()
loadtype = args.loadtype

inputfile = ""
if loadtype == "x_x":
    inputfile = "energy.data"
elif loadtype == "motivation":
    inputfile = "motivation.data"
else:
    inputfile = "foreign.data"

 # set width of bar
barWidth = .15

benchmark_records=kutils.read_csv_as_array(inputfile)
#Each record represents a benchmark run
#The columns are ordered as : benchmark name, instance one energy, instance two energy, other apps

benchmark_names=[]
instance_a=[]
instance_b=[]
raw_1=[]
raw_2=[]
other=[]

instance_a_pear=[]
instance_b_pear=[]

instance_a_std=[]
instance_b_std=[]
raw_1_std=[]
raw_2_std=[]
other_std=[]


i=0
while i < len(benchmark_records):
    record = benchmark_records[i]
    if(record[0].startswith("#")):
        i=i+1
        continue

    if loadtype == "x_x":
        benchmark_names.append(record[0]+"\n"+record[3])
        instance_a.append(float(record[1]))
        instance_a_std.append(float(record[2]))

        instance_b.append(float(record[4]))
        instance_b_std.append(float(record[5]))

        raw_1.append(float(record[7]))
        raw_1_std.append(float(record[8]))

        raw_2.append(float(record[10]))
        raw_2_std.append(float(record[11]))

        other.append(float(record[13]))
        other_std.append(float(record[14]))

        instance_a_pear.append(float(record[15]))
        instance_b_pear.append(float(record[16]))

    elif loadtype == "motivation":
        benchmark_names.append(record[0]+"\n"+record[2])
        instance_a.append(float(record[1]))
        instance_b.append(float(record[3]))
        raw_1.append(float(record[5]))
        raw_2.append(float(record[7]))

    else:
        benchmark_names.append(record[0])
        instance_a.append(float(record[1]))
        raw_1.append(float(record[3]))
        other.append(float(record[5]))
        instance_a_pear.append(float(record[6]))

    i=i+1



# Set position of bar on X axis
r3 = np.arange(len(instance_a))
r2 = [x - barWidth for x in r3]
r1 = [x - barWidth for x in r2]

r4 = [x + barWidth for x in r3]
r5 = [x + barWidth for x in r4]
    
colors=['silver','gray', 'lightgray', 'darkgray']
#colors=['silver', 'dimgray', 'gray', 'gray', 'lightgray', 'darkgray']

labelsize=8

# Make the plot
if loadtype == "x_x":
    r2 = np.arange(len(instance_a))
    r1 = [x - barWidth for x in r2]
    r3 = [x + barWidth for x in r2]


    print(instance_a)
    print(instance_a_std)
    print(other)

    plt.figure(figsize=(10,2))
    bar1 = plt.bar(r1, instance_a, color="silver", width=barWidth, edgecolor='white',label="App A")
    bar2 = plt.bar(r2, instance_b, color="dimgray", width=barWidth, edgecolor='white',label="App B")

    for i,rect in enumerate(bar1):
        height = rect.get_height()
        plt.text(rect.get_x() + rect.get_width()/2.0 - 0.05, height, ("%.2f" % instance_a_pear[i]).lstrip('0'), ha='center', va='bottom',fontsize=8.5)

    for i,rect in enumerate(bar2):
        height = rect.get_height()
        plt.text(rect.get_x() + rect.get_width()/2.0 + 0.05, height + 20, ("%.2f" % instance_b_pear[i]).lstrip('0'), ha='center', va='bottom',fontsize=8.5)

elif loadtype == "motivation":
    ax = plt.figure(figsize=(5,2))
    plt.bar(r1, instance_a, color="lightgray", width=barWidth, edgecolor='white',label="App A")
    plt.bar(r2, instance_b, color="silver", width=barWidth, edgecolor='white',label="App B")
    plt.bar(r3, raw_1,     color="darkgray", width=barWidth, edgecolor='white',label="Unattributed A")
    plt.bar(r4, raw_2,     color="dimgray", width=barWidth, edgecolor='white',label="Unattributed B")
else:
    labelsize=10
    plt.figure(figsize=(5,2.0))
    bar1 = plt.bar(r2, instance_a, color="silver", width=barWidth, edgecolor='white',label="App A")
    plt.bar(r3, other,     color="gray", width=barWidth, edgecolor='white',label="Foreign Load")
    #plt.bar(r4, raw_1,     color="dimgray", width=barWidth, edgecolor='white',label="Unattributed A")

    for i,rect in enumerate(bar1):
        height = rect.get_height()
        plt.text(rect.get_x() + rect.get_width()/2.0 - 0.05, height, ("%.2f" % instance_a_pear[i]).lstrip('0'), ha='center', va='bottom',fontsize=8.5)

     
plt.xticks(fontsize=labelsize,rotation=0)
plt.yticks(fontsize=labelsize)
plt.ylabel('Energy (J)',fontsize=12)
plt.xlabel('Benchmarks',fontsize=12)
plt.xticks([r for r in range(len(instance_a))], benchmark_names)
plt.legend(prop={'size':7},loc=2)

ax=plt.gca()
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)

# Create legend & Show graphic
plt.show()
plt.plot()
#pp.savefig(plt)
if loadtype == "x_x":
    plt.savefig('./co-app.pdf', bbox_inches='tight')
elif loadtype == "motivation":
    plt.savefig('./co-app-motivation.pdf', bbox_inches='tight')
else:
    plt.savefig('./foreign-app.pdf', bbox_inches='tight')

#f.savefig("foo.pdf", bbox_inches='tight')
