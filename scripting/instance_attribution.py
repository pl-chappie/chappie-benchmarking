#!/usr/bin/python3

import sys

import os
import argparse
import numpy as np
import pandas as pd
import kutils

cvs_data=[]
ITER_LEN=20

def extract_total_app_energy(path):
    results = np.sort([f for f in os.listdir(path) if 'result' in f and 'clean' not in f])
    #Each Iteration will be treated differently ... so stay tuned!
    totals=[]

    for result in results:
        total=0.0
        lines = kutils.read_csv_as_array(path+"/"+result)
        lines = lines[1:-1]
        for line in lines:
            if(line[4] != ""):
                total = total + float(line[4])
        
        totals.append(total)
    
    return totals

def extract_raw_app_energy(path):
    results = np.sort([f for f in os.listdir(path) if 'trace' in f and 'clean' not in f])
    #Each Iteration will be treated differently ... so stay tuned!
    totals=[]

    for result in results:
        total=0.0
        lines = kutils.read_csv_as_array(path+"/"+result)
        lines = lines[1:-1]
        for line in lines:
            total += float(line[0]) + float(line[2])
        
        totals.append(total)
    
    return totals



parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-path', action="store", default=".", dest="path")
parser.add_argument('-loadtype', action="store", default=".", dest="loadtype")
args = parser.parse_args()
path = os.path.expanduser(args.path)
loadtype = args.loadtype

experiments = np.sort([f for f in os.listdir(path)])
csv_data=[]

for experiment in experiments:
    ##Calculate the energy contribution of each experiment
    exp_benchmarks_path = path+"/"+experiment
    print("Processing experiment %s" %(exp_benchmarks_path))
    exp_benchmarks = np.sort([f for f in os.listdir(exp_benchmarks_path)])
    exp_benchmarks = filter(lambda x: x != "graphs", exp_benchmarks)
    print(exp_benchmarks)
    
    #experiment_readings = {}
    attributed_eng=[]
    attributed_eng_std=[]
    raw_eng=[]
    raw_eng_std=[]

    bench_names=[]
    total_attributed_energy = 0
    raw_energy = 0

    attributed_energy_arr_1 = None
    attributed_energy_arr_2 = None


    raw_energy_arr = None
    max_raw_energy = 0

    if loadtype == 'x_x':
        for exp_benchmark in exp_benchmarks: 
                print("....... Processing Benchmark %s" %(exp_benchmark))
                total_app_arr      = np.array(extract_total_app_energy(path+"/"+experiment+"/"+exp_benchmark))
                if (attributed_energy_arr_1 is None):
                    attributed_energy_arr_1 = total_app_arr
                else:
                    attributed_energy_arr_2 = total_app_arr

                total_app_energy      = np.mean(total_app_arr)
                total_app_std = np.std(total_app_arr)

                fixedname = exp_benchmark.split("_")[0]

                bench_names.append(fixedname) 
                attributed_eng.append(total_app_energy)
                attributed_eng_std.append(total_app_std)

                raw_app_energy_arr      = np.array(extract_raw_app_energy(path+"/"+experiment+"/"+exp_benchmark))
                raw_app_energy = np.mean(raw_app_energy_arr)
                raw_app_energy_std = np.std(raw_app_energy_arr)

                raw_eng.append(raw_app_energy)
                raw_eng_std.append(raw_app_energy_std)

                if (raw_app_energy > max_raw_energy):
                    max_raw_energy = raw_app_energy
                    raw_energy_arr = raw_app_energy_arr
        
        other_eng_arr = []
        for i in range(len(attributed_energy_arr_1)):
            other_eng_arr.append(raw_energy_arr[i]-attributed_energy_arr_1[i]-attributed_energy_arr_2[i])


        other_eng_np = np.array(other_eng_arr)
        other_eng = np.mean(other_eng_np)
        other_eng_std = np.std(other_eng_np)

        indx = 0
        cvs_entry = "%s,%f,%f,%s,%f,%f,%s,%f,%f,%s,%f,%f,%s,%f,%f" \
            %(bench_names[0],attributed_eng[0],attributed_eng_std[0],
              bench_names[1],attributed_eng[1],attributed_eng_std[1],
              bench_names[0],raw_eng[0],raw_eng_std[0],
              bench_names[1],raw_eng[1],raw_eng_std[1],
              "other",other_eng,other_eng_std)

        cvs_data.append(cvs_entry)
        kutils.write_file("energy.data",cvs_data)
        
    if loadtype == 'foreign':
        for exp_benchmark in exp_benchmarks: 
                print("....... Processing Benchmark %s" %(exp_benchmark))

                print(path+"/"+experiment+"/"+exp_benchmark)

                total_app_energy      = sum(extract_total_app_energy(path+"/"+experiment+"/"+exp_benchmark)) / ITER_LEN
                total_attributed_energy += total_app_energy

                fixedname = exp_benchmark.split("_")[0]

                bench_names.append(fixedname) 
                attributed_eng.append(total_app_energy)

                raw_app_energy      = sum(extract_raw_app_energy(path+"/"+experiment+"/"+exp_benchmark)) / ITER_LEN
                if (raw_app_energy > raw_energy):
                    raw_energy = raw_app_energy
                raw_eng.append(raw_app_energy)
        
        other_eng = raw_energy - total_attributed_energy

        indx = 0
        cvs_entry = "%s,%f,%s,%f,%s,%f" \
            %(bench_names[0],attributed_eng[0],bench_names[0],raw_eng[0],"other",other_eng)

        cvs_data.append(cvs_entry)
        kutils.write_file("foreign.data",cvs_data)


        


            
    










      
quit()
