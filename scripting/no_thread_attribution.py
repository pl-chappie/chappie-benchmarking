#!/usr/bin/python3

import sys
import os
import argparse
import numpy as np
import pandas as pd
import kutils

PKG_INDX=4
EPOCH_INDX=0
STACK_INDX=6

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-path', action="store", default="processed_data/eclipse", dest="path")
parser.add_argument('-top', action="store", default="top.data", dest="top")
parser.add_argument('-dest', action="store", default="top_methods_no_threads.csv", dest="dest")
args = parser.parse_args()
path = os.path.expanduser(args.path)
top_path = os.path.expanduser(args.top)
dest = os.path.expanduser(args.dest)

#This will be populated from the PDF or Top 10 files or whaever
top_methods = kutils.read_csv_as_array(top_path)
old_methods_energy={}
top_methods_energy={}

for line in top_methods:
    method_name=line[0].strip()
    top_methods_energy[method_name]=0.0
    old_methods_energy[method_name]=float(line[-1].strip())


results = np.sort([f for f in os.listdir(path) if 'result' in f and 'clean' not in f])

for result in results:
        print("Processing %s" %(result))
        lines = kutils.read_csv_as_array(path+"/"+result)
	lines = lines[1:-1]
	previous_epoch=-1;
	previous_energy=0.0;
	method_winner="";
        for line in lines:
			epoch = int(line[EPOCH_INDX])
			
			if (epoch != previous_epoch and previous_epoch != -1):
			        if not(method_winner in top_methods_energy.keys()):
                                        continue
                                top_methods_energy[method_winner]+=previous_energy
				method_winnter=""
				previous_energy=0
				
			previous_epoch=epoch
			energy=float(line[PKG_INDX])
			previous_energy+=energy
			
			#Check method winner
			#This is the most important step in the world
			stack_methods=line[STACK_INDX].split(";")
			
			for stack_method  in stack_methods:
                                methods = stack_method.split(".")
                                if(len(methods) > 1):
                                    stack_method = ".".join(methods[-2:])

                                if(stack_method in top_methods_energy.keys()):
                                        method_winner=stack_method

csv_data=[]
for k,v in top_methods_energy.items():
    if(v>0):
        csv_entry="%s,%f,%f" %(k,v,old_methods_energy[k])
        csv_data.append(csv_entry)


kutils.write_file(dest,csv_data)


