#!/bin/bash

PATH="./processed_data"
DEST="processed_data"

benchmarks=(
avrora 
#batik
#eclipse 
#h2 
#jython 
#luindex 
#lusearch 
#pmd 
#sunflow 
#tomcat 
#  tradebeans 
#  tradesoap 
#xalan
)

for benchmark in "${benchmarks[@]}"; do
  echo $benchmark

  #Method Attribution script
  /usr/bin/python3.5 method-energy-proportion.py -path="./processed_data/${benchmark}" -benchmark=${benchmark} -kind = "java" -destination="./processed_data/graphs/${benchmark}/"
  #/usr/bin/python3.5 ./local-graphs/method-energy-proportion.py -path=$PATH/$benchmark -benchmark=$benchmark -destination=$DEST/graphs/${benchmark}

  #/usr/bin/python3.5 ./local-graphs/cfa-method-energy-proportion.py -path="./processed_data/${benchmark}" -benchmark=${benchmark} -destination="./processed_data/graphs/${benchmark}/"

  #/usr/bin/python3.5 ./local-graphs/cfa-method-energy-pie.py -path="./processed_data/${benchmark}" -benchmark=${benchmark} -destination="./processed_data/graphs/${benchmark}/"
done

