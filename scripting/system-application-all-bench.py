import os
import argparse
import pandas as pd
import numpy as np
import matplotlib as mpl
#if os.environ.get('DISPLAY','') == '':
#    print('no display found. Using non-interactive Agg backend')
mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-benchmark', action="store", default="sunflow", dest="benchmark")
parser.add_argument('-path', action="store", default="~/Desktop/", dest="path")
parser.add_argument('-destination', action="store", default="~/Desktop/", dest="destination")
args = parser.parse_args()

path = os.path.expanduser(args.path)
benchmark = args.benchmark
destination = os.path.expanduser(args.destination)

benchmark = (list(benchmark.split(",")))
if not os.path.exists(destination):
    os.makedirs(destination)

#with PdfPages(destination+'/system-application-attribution-all-bench.pdf') as pdf:
newDf = pd.DataFrame(columns=['benchmark', 'package_system', 'dram_system', 'package_sock1', 'package_sock2',
                              'dram_sock1', 'dram_sock2', 'total'])
count = 0
for bench in benchmark:
    bench = bench.strip()
    path = '{}/{}/'.format(os.path.expanduser(args.path), bench)
    numOfFiles = 0
    for entry in os.scandir(path):
        if entry.is_file() and "system-application" in entry.name:
            numOfFiles += 1

    for i in range(numOfFiles):
        df = pd.read_csv(path+'/system-application_'+str(i)+'.csv')
        ind = df.index.max()
        package_system = df['sys_package'].sum()
        dram_system = df['sys_dram'].sum()

        package_sock1 = df['sock1_app_package'].sum()
        package_sock2 = df['sock2_app_package'].sum()
        dram_sock1 = df['sock1_app_dram'].sum()
        dram_sock2 = df['sock2_app_dram'].sum()
        total = package_system + dram_system + package_sock1 + package_sock2 + dram_sock1 + dram_sock2
        newDf.loc[count] = [bench, package_system, dram_system, package_sock1, package_sock2, dram_sock1, dram_sock2, total]
        count+=1


stds = newDf.groupby(['benchmark'], as_index=False)['total'].agg(['std']).reset_index()
stds.set_index('benchmark',inplace=True)
newDf = newDf.groupby(['benchmark'], as_index=False).mean()
newDf.set_index('benchmark', inplace=True)

print(newDf)


newDf = newDf.drop(['total'],axis=1)
newDf = newDf.rename(columns={'package_system': 'System CPU', 'dram_system': 'System DRAM', 'package_sock1': 'App CPU 1', 'package_sock2': 'App CPU 2', 'dram_sock1': 'App DRAM 1', 'dram_sock2': 'App DRAM 2'})

# Disable system package and system application
newDf = newDf.drop(columns=['System CPU','System DRAM'])

#newDf['std'] = stds['std']

#colors=['gainsboro','lightgrey', 'silver', 'darkgrey', 'gray', 'dimgrey']
colors=['gainsboro', 'dimgrey', 'silver', 'gray', 'lightgray', 'darkgray']
#colors.reverse()

newDf = newDf.rename(index={'ALSMatrixFactorization':'GraphChi-ALS', 'ConnectedComponents':'GraphChi-CC', 'Pagerank':'GraphChi-PR'})



ax = newDf.plot.bar(stacked=True,figsize=(5, 2.5),color=colors,legend=False)
#for container, hatch in zip(ax.containers, ("///", "///")):
#    for patch in container.patches:
#        patch.set_hatch(hatch)

handles,labels = ax.get_legend_handles_labels()

handles.reverse()
labels.reverse()

plt.legend(handles,labels,loc='best',prop={'size':7})

plt.xticks(fontsize=8,rotation=45)
plt.yticks(fontsize=8)
plt.xlabel('Benchmarks', fontsize=12)
#plt.yscale('log')
plt.ylabel('Energy (J)', fontsize=12)
#plt.show()

plt.savefig(destination+'/system-application-attribution-all-bench.svg', bbox_inches='tight')
plt.close()
