import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import kutils
from matplotlib.backends.backend_pdf import PdfPages
import argparse
import os

 # set width of bar
barWidth = .1

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-file', action="store", default="diff.data", dest="file")
args = parser.parse_args()
file = os.path.expanduser(args.file)


records=kutils.read_csv_as_array(file)

method_names=[]
instance_a=[]
instance_b=[]
others=[]

i=0
while i < len(records):
    record = records[i]
    if(record[0].startswith("#")):
        i=i+1
        continue

    if(float(record[1]) ==0 or float(record[2]) == 0):
        i=i+1
        continue


    if(float(record[1]) >= 5 or float(record[2]) >= 5):
        i=i+1
        continue

    method_names.append("")
    instance_a.append(float(record[1]))
    instance_b.append(float(record[2]))
    i=i+1
    if(i>20):
        break


# Set position of bar on X axis
print(instance_a)
print(instance_b)
r1 = np.arange(len(instance_a))
r2 = [x + barWidth for x in r1]
plt.bar(r1, instance_a, color='#7f6d5f', width=barWidth, edgecolor='white',label="Before")
plt.bar(r2, instance_b, color='#557f2d', width=barWidth, edgecolor='white',label="After")

plt.xlabel('Methods', fontweight='bold')
plt.ylabel('Energy', fontweight='bold')
plt.xticks([r + barWidth for r in range(len(instance_a))], method_names)

plt.legend()
plt.show()
plt.plot()
#pp.savefig(plt)
plt.savefig("diff", dpi=1000)

#f.savefig("foo.pdf", bbox_inches='tight')
