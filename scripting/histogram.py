import pandas as pd
import matplotlib.pyplot as plt
import  seaborn as sns
from matplotlib.backends.backend_pdf import PdfPages

benchmark = 'graphchi'
path = '~/Desktop/graphchi_logs/'
with PdfPages(benchmark+'-socket-wise-data.pdf') as pdf:
    for i in range(10):
        df = pd.read_csv(path+'chappie.thread.'+str(i)+'.csv')
        for choice in ['Reference Handler', 'Chaperone', 'main', 'Signal Dispatcher', 'Finalizer']:
            df = df[df['thread'] != choice]

        #socket1
        data_socket1 = df[df.core < 20]

        #socket2
        data_socket2 = df[df.core >= 20]

        fig, ax = plt.subplots(4, 2, figsize=(15, 20))
        ax = ax.ravel()
        fig = plt.figure(figsize=(15, 20))
        iter =0
        count =1
        #data.to_csv(path+'processed_chappie.thread.'+str(i)+'.csv')
        for data in [data_socket1, data_socket2]:
            #fig = plt.figure(figsize=(15,10))
            a = data['core']
            b = data['package']
            c = data['dram']
            d = data['bytes']

            fig.suptitle('Histogram from chappie.thread.' + str(i) + '.csv')

            ax[(2 * iter)] = fig.add_subplot(4, 2, (2 * iter) + 1)
            sns.distplot(a, bins=20, kde=False, label='core', color='r')
            plt.legend(loc='upper right')
            plt.xlabel('Core (Socket ' +str(count)+')')
            plt.ylabel('Frequency ')

            ax[(2 * iter) + 1] = fig.add_subplot(4, 2, (2 * iter) + 2)
            sns.distplot(b, bins=100, kde=False, label='Package', color='r')
            plt.legend(loc='upper right')
            plt.xlabel('Package (Socket ' +str(count)+')')
            plt.ylabel('Frequency')

            ax[(2 * iter) + 2] = fig.add_subplot(4, 2, (2 * iter) + 3)
            sns.distplot(c, bins=100, kde=False, label='DRAM', color='r')
            plt.legend(loc='upper right')
            plt.xlabel('DRAM (Socket ' +str(count)+')')
            plt.ylabel('Frequency')

            ax[(2 * iter) + 3] = fig.add_subplot(4, 2, (2 * iter) + 4)
            sns.distplot(d, bins=250, kde=False, label='bytes', color='r')
            plt.legend(loc='upper right')
            plt.xlabel('Bytes (Socket ' +str(count)+')')
            plt.ylabel('Frequency')

            iter += 2
            count += 1

        pdf.savefig()
        plt.close()
