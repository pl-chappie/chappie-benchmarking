import pandas as pd
import os
import argparse
import math
import matplotlib as mpl

if os.environ.get('DISPLAY', '') == '':
    print('no display found. Using non-interactive Agg backend')
    mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-benchmark', action="store", default="tpcc", dest="benchmark")
parser.add_argument('-path', action="store", default="~/Desktop/tpcc/", dest="path")
parser.add_argument('-destination', action="store", default="~/Desktop/tpcc/test",
                    dest="destination")

args = parser.parse_args()

path = os.path.expanduser(args.path)
benchmark = args.benchmark
destination = os.path.expanduser(args.destination)
if not os.path.exists(destination):
    os.makedirs(destination)

numOfFiles = 0
for entry in os.scandir(path):
    if entry.is_file() and "result_" in entry.name:
        numOfFiles += 1

for i in range(numOfFiles):
    with PdfPages('{}//energy-per-unit-time_{}_{}.pdf'.format(destination, benchmark, i)) as pdf:
        df = pd.read_csv(path + '/result_' + str(i) + '.csv')
        df = df.loc[df['core'] != -1]

        df = df[df['core'] == df['core']]

        df['energy'] = df['package'] + df['dram']
        df = df.groupby(['epoch', 'core'], as_index=False, sort=False)['energy'].sum()

        # df['energy_socket'] = df.apply(lambda row: 1 if row['core'] < 20 else 2, axis=1)

        df_socket1 = df.loc[df['core'] < 20]
        df_socket2 = df.loc[df['core'] >= 20]

        df_socket1 = df_socket1.groupby(['epoch'], as_index=False, sort=False)['energy'].sum()
        df_socket1.rename(columns={'energy': 'energy_socket1'}, inplace=True)

        df_socket2 = df_socket2.groupby(['epoch'], as_index=False, sort=False)['energy'].sum()
        df_socket2.rename(columns={'energy': 'energy_socket2'}, inplace=True)

        result = pd.merge(df_socket1, df_socket2, on='epoch', how='outer')
        result = result.fillna(0)
        result.set_index('epoch', inplace=True)
        result = result.sort_values('epoch')
        bins = math.ceil(len(df) / 50)
        print('Bin size: {}'.format(math.ceil(bins)))
        if len(df) < bins:
            split = np.array_split(result, 1)
        else:
            split = np.array_split(result, len(df)/bins)

        newDf = pd.DataFrame(columns=['epoch', 'energy_socket1', 'energy_socket2'])
        for frame in range(len(split)):
            time = split[frame].index.max()
            energy_socket1 = split[frame]['energy_socket1'].sum()
            energy_socket2 = split[frame]['energy_socket2'].sum()
            newDf.loc[frame] = [time, energy_socket1, energy_socket2]

        newDf.set_index('epoch', inplace=True)

        ax = newDf.loc[:, ['energy_socket1', 'energy_socket2']].plot.bar(stacked=True, figsize=(30, 20), color=['red','green'])
        plt.legend(loc='best', fontsize=25)
        plt.xlabel('Elapsed Time ({} ms as 1 Unit)'.format(bins*4), fontsize=25)
        plt.ylabel('Energy (J)', fontsize=25)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)

        plt.tight_layout()
        pdf.savefig()
        #plt.show()
        plt.close()
