#!/bin/bash

# usage: Invoke with old to skip the method alignment, invoke with new to perform it:
#   ./analyze_dacapo.sh old
#   ./analyze_dacapo.sh new


benchmarks=(
avrora  # core issue
#batik
#eclipse 
#h2 
#jython  # core issue
#luindex 
#lusearch 
#pmd 
#sunflow 
#  tomcat     # doesn't run
#  tradebeans # doesn't run
#  tradesoap  # doesn't run
#xalan
)

for benchmark in "${benchmarks[@]}"; do
  nlogs=`find ./chappie.dacapo/${benchmark} -maxdepth 1 -type f -name '*.thread.*.csv' | wc -l`
  nlogs=$((nlogs-1))
  d=./chappie.dacapo/${benchmark}
  for i in `seq 0 $nlogs`; do
    cp $d/saved.$i.csv $d/chappie.thread.$i.csv
  done
done
