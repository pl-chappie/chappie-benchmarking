import pandas as pd
import os
import argparse
import re
import matplotlib as mpl

if os.environ.get('DISPLAY', '') == '':
    print('no display found. Using non-interactive Agg backend')
    mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-benchmark', action="store", default="sunflow", dest="benchmark")
parser.add_argument('-path', action="store", default="~/Desktop/sunflow/processed_data", dest="path")
parser.add_argument('-destination', action="store", default="~/Desktop/sunflow/graphs",
                    dest="destination")

args = parser.parse_args()

path = os.path.expanduser(args.path)
benchmark = args.benchmark
destination = os.path.expanduser(args.destination)
if not os.path.exists(destination):
    os.makedirs(destination)

numOfFiles = 0
for entry in os.scandir(path):
    if entry.is_file() and "method_hotness" in entry.name:
        numOfFiles += 1

bigData = pd.DataFrame(columns=['method', 'hits'])
total_hits = 0

for i in range(3, numOfFiles):
    df = pd.read_csv(path + '/method_hotness_' + str(i) + '.csv')
    bigData = bigData.append(df, ignore_index=True)
    bigData = bigData.fillna(value=0)

bigData['method'] = bigData['method'].apply(lambda x: re.sub(r"\(+.*\)+", "", x))
bigData['method'] = bigData['method'].apply(lambda x: ((x.split("."))[len(x.split("."))-2]) + '.'
                                                      +((x.split("."))[len(x.split("."))-1]))
bigData = bigData.loc[bigData['method'] != 'Thread.getStackTrace']
bigData = bigData.groupby('method')['hits'].sum().reset_index(name ='hits')
bigData = bigData.sort_values('hits', ascending=False).head(10)
print(bigData)

with PdfPages(destination + '/method-time-ranking-' + benchmark + '.pdf') as pdf:
    firstPage = plt.figure(figsize=(15,10))
    firstPage.clf()
    txt = 'method-time-ranking-' + benchmark
    firstPage.text(0.5,0.5,txt, transform=firstPage.transFigure, size=24, ha="center")
    pdf.savefig()
    plt.close()

    x = np.array(bigData['method'])
    ax = bigData.plot(kind='bar', x='method', y=['hits'], figsize=(15, 10), color='indigo')
    ax.set_xticklabels(bigData['method'])
    plt.ylabel('Number of Hits')
    plt.xlabel('Method')
    plt.xticks(rotation=90)
    plt.tight_layout()
    pdf.savefig()
    #plt.show()
    plt.close()

