#!/usr/bin/python3

import numpy as np
import pandas as pd
import argparse
import os
import re

method_kinds = ['filtered','shallow','deep','library']
#method_kinds = ['deep']


def trimToClassAndMethod(x):
    s = re.sub(r"\(+.*\)+", "", x)
    s2 = ((s.split("."))[len(s.split(".")) - 2]) + '.' + ((s.split("."))[len(s.split(".")) - 1])
    return s2
    #return x

def trimToPackage(x):
    s = re.sub(r"\(+.*\)+", "", x)
    s = s.split(".")
    s.pop()
    s.pop()
    nx = ".".join(s)
    return nx

def trimToClass(x):
    s = x.split(".")[0]
    return s

def filter_java(x):
    tok = x.split(";")
    nx = None
    if tok[0].startswith("java."):
        i = 0
        while i < len(x):
            if not tok[i].startswith("java."):
                break
            i += 1
        nx = tok[i:-1]
    else:
        nx = tok

    nx = ";".join(nx)
    return nx


def process_energy(data):
    data = data.groupby(['method', 'socket'])[['package', 'dram']].sum().reset_index()

    package = data.reset_index().pivot(index='method', columns='socket', values='package').fillna(0).reset_index().rename(
            columns={1: 'package1', 2: 'package2'})
    dram = data.reset_index().pivot(index='method', columns='socket', values='dram').fillna(0).reset_index().rename(
        columns={1: 'dram1', 2: 'dram2'})
    data = pd.merge(package, dram, on='method')
    data['energy'] = data[[col for col in data.columns if '1' in col or '2' in col]].sum(axis=1)
    return data


def process_methodkind(data, kind):
    # Filter out system thread and Chappie from analysis
    data = data[~data['thread'].str.contains('Chaperone')]

    forhotness = data.copy() 
    data = data.groupby(['stack', 'socket'])[['package', 'dram']].sum().reset_index()

    # Perform per-kind processesing

    if kind is 'filtered':
        data['method'] = data['stack'].str.split(';')
        data = data[~data['stack'].str.startswith('java.') & ~data['stack'].isin(['end', ''])]

        forhotness['method'] = forhotness['stack'].str.split(';')
        forhotness = forhotness[~forhotness['stack'].str.startswith('java.') & ~forhotness['stack'].isin(['end', ''])]

    elif kind is 'shallow':
        data['method'] = data['stack'].str.split(';')
        data = data[~data['stack'].isin(['end', ''])]

        forhotness['method'] = forhotness['stack'].str.split(';')
        forhotness = forhotness[~forhotness['stack'].isin(['end', ''])]

    elif kind is 'deep':
        data['stack'] = data['stack'].apply(filter_java)
        data['method'] = data['stack'].str.split(';')
        data = data[~data['stack'].isin(['end', ''])]

        forhotness['stack'] = forhotness['stack'].apply(filter_java)
        forhotness['method'] = forhotness['stack'].str.split(';')
        forhotness = forhotness[~forhotness['stack'].isin(['end', ''])]

    elif kind is 'library':
        data['method'] = data['stack'].str.split(';')
        data = data[data['stack'].str.startswith('java.') & ~data['stack'].isin(['end', ''])]

        forhotness['method'] = forhotness['stack'].str.split(';')
        forhotness = forhotness[forhotness['stack'].str.startswith('java.') & ~forhotness['stack'].isin(['end', ''])]
    else:
        print("Incorrect method kind!")
        exit()

    package = data.copy()
    package['method'] = package['method'].map( lambda x: trimToPackage(x[0]) )

    method = data.copy()
    method['method'] = method['method'].map( lambda x: trimToClassAndMethod(x[0]) ) 

    forhotness['method'] = forhotness['method'].map( lambda x: trimToClassAndMethod(x[0]) ) 
    forhotness = forhotness.groupby(['method']).size().reset_index(name='hits')
    forhotness.to_csv('{}/{}_hotness_{}.csv'.format(destination, kind, i), index=False)

    clazz = method.copy()
    clazz['method'] = clazz['method'].map( lambda x: trimToClass(x) )

    method = process_energy(method)
    method.to_csv('{}/{}_attribution_{}.csv'.format(destination, kind ,i), index=False)

    clazz = process_energy(clazz)
    clazz.to_csv('{}/{}_class_{}.csv'.format(destination, kind ,i), index=False)

    package = process_energy(package)
    package.to_csv('{}/{}_package_{}.csv'.format(destination, kind ,i), index=False)

    cfa2 = data.copy()
    cfa2['method'] = cfa2['method'].map( lambda x: (trimToClassAndMethod(x[0]) + "|" + trimToClassAndMethod(x[1]) + "|" + trimToClassAndMethod(x[2])) if len(x) > 2 else trimToClassAndMethod(x[0]))
    cfa2 = process_energy(cfa2)
    cfa2.to_csv('{}/{}_2cfa_{}.csv'.format(destination, kind ,i), index=False)

    return

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Argument Parser')
    parser.add_argument('-benchmark', action="store", default="sunflow", dest="benchmark")
    parser.add_argument('-path', action="store", default="~/Desktop/sunflow", dest="path")
    parser.add_argument('-destination', action="store", default="~/Desktop/sunflow/processed_data",
                        dest="destination")
    args = parser.parse_args()

    path = os.path.expanduser(args.path)
    benchmark = args.benchmark
    destination = os.path.expanduser(args.destination)
    if not os.path.exists(destination):
        os.makedirs(destination)

    threads = np.sort([f for f in os.listdir(destination) if 'result' in f])

    for i, thread in enumerate(threads):
        filename = str(thread)
        thread = pd.read_csv(os.path.join(destination, thread))

        for k in method_kinds:
            process_methodkind(thread,k)
