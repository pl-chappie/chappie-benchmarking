#!/bin/bash

# usage: Invoke with old to skip the method alignment, invoke with new to perform it:
#   ./analyze_dacapo.sh old
#   ./analyze_dacapo.sh new

#if [[ $# -ne 1 ]]; then
#  echo "usage: $0 [CHAPPIE VERSION]"
#  exit
#fi

pdata="./processed_data"

if [[ $# -eq 3 ]]; then
	p="$2"
fi

echo "Using pdatadir $pdata"
experiments=$(ls $pdata)
echo $experiments
for expt in $experiments; do
  echo "main experiment $expt"
	benchmarks=$(ls $pdata/$expt)
  echo "benchmarks $benchmarks"
	experiment_name=$(echo "$path" | rev | cut -d / -f 1|rev)
	#mkdir $pdata/$experiment_name

  echo "benchmark, pearsons correlation, spearman rank" > ./$pdata/$expt/graphs/filtered.txt

	for bench in $benchmarks; do
    echo "Looking at $pdata/$expt/$bench"
    if [[ $bench == "graphs" ]]; then
      continue
    fi

    /usr/bin/python3.5 ./method-energy-proportion.py -path="./$pdata/$expt/$bench" -benchmark=${bench} -destination="./$pdata/$expt/graphs/$bench/" -kind="filtered" -skip=true
	done
done

	#do
	#	if [ -d $path/${benchmark}/chappie.dacapo-9.12-bach ];
	#	then
	#		mv $path/${benchmark}/chappie.dacapo-9.12-bach/* $path/${benchmark}/
	#	fi
#
#		if [[ $1 = "new" ]]; then
#			echo  "Benchmark:$path/${benchmark}"		
#			nlogs=`find $path/${benchmark} -maxdepth 1 -type f -name '*.thread.*.csv' | wc -l`
#			nlogs=$((nlogs-1))
#			d=$path/${benchmark}
#			echo "Processing $benchmark"
#			echo $nlogs		
#			for i in `seq 0 $nlogs`; do
#				./opt-align.rb -c $d/chappie.thread.$i.csv -h $d/log.$i.hpl > $d/aligned.$i.csv
#			done
#			for i in `seq 0 $nlogs`; do
#			 	 mv $d/chappie.thread.$i.csv $d/saved.$i.csv
#			 	 mv $d/aligned.$i.csv $d/chappie.thread.$i.csv
#			done
#		fi
		#/usr/bin/python3.5 data_processing.py -path="$path/${benchmark}" -benchmark=${benchmark} -destination="$pdata/${benchmark}"
				#Method Attribution script
		#/usr/bin/python3.5 method-attribution.py -path="$path/${benchmark}" -benchmark=${benchmark} -destination="$pdata/${benchmark}"
#
#    /usr/bin/python3.5 ./scripting/method-energy-proportion.py -path="./processed_data/${benchmark}" -benchmark=${benchmark} -destination="./processed_data/graphs/${benchmark}/" -kind="filtered"
		
#		mv $pdata/$benchmark $pdata/$experiment_name		 








