import os
import argparse
import pandas as pd
import numpy as np
import  math
import matplotlib as mpl
if os.environ.get('DISPLAY','') == '':
    print('no display found. Using non-interactive Agg backend')
    mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-benchmark', action="store", default="tpcc", dest="benchmark")
parser.add_argument('-path', action="store", default="~/Desktop/tpcc/", dest="path")
parser.add_argument('-destination', action="store", default="~/Desktop/tpcc/test/", dest="destination")
args = parser.parse_args()

path = os.path.expanduser(args.path)
benchmark = args.benchmark
destination = os.path.expanduser(args.destination)
if not os.path.exists(destination):
    os.makedirs(destination)

numOfFiles = 0
for entry in os.scandir(path):
    if entry.is_file() and "system-application_" in entry.name:
        numOfFiles += 1

for i in range(numOfFiles):
    with PdfPages('{}/physical-attribution-{}_{}.pdf'.format(destination, benchmark, i)) as pdf:

        print('physical-attribution-{}_{}.pdf'.format(benchmark, i))

        df = pd.read_csv(path+'/system-application_'+str(i)+'.csv')
        df = df[['time', 'sock1_app_package', 'sock1_app_dram', 'sock2_app_package', 'sock2_app_dram']]

        bins = math.ceil(len(df) / 50)
        if len(df) < 25:
            split = np.array_split(df, 1)
        else:
            split = np.array_split(df, len(df)/bins)
        newDf = pd.DataFrame(columns=['time', 'package_sock1', 'package_sock2', 'dram_sock1', 'dram_sock2'])
        for frame in range(len(split)):
            ind = split[frame].index.max()
            time = split[frame].loc[ind]['time']
            package_sock1 = split[frame]['sock1_app_package'].sum()
            package_sock2 = split[frame]['sock2_app_package'].sum()
            dram_sock1 = split[frame]['sock1_app_dram'].sum()
            dram_sock2 = split[frame]['sock2_app_dram'].sum()
            newDf.loc[frame] = [time, package_sock1, package_sock2, dram_sock1, dram_sock2]

        newDf.set_index('time', inplace=True)
        plot_params = ['package_sock1', 'package_sock2', 'dram_sock1', 'dram_sock2']
        newDf.loc[:, plot_params].plot.bar(stacked=True, figsize=(30, 20))
        plt.legend(loc='best', fontsize=20)
        plt.xlabel('Elapsed Time ({} ms as 1 Unit)'.format(bins * 4), fontsize=25)
        plt.ylabel('Energy (J)', fontsize=25)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)

        pdf.savefig()
        plt.close()
