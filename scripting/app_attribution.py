#!/usr/bin/python3

import sys

import os
import argparse
import numpy as np
import pandas as pd
import kutils

def read_jiffies_as_array(path):
    temp_array=[]
    with open(path) as f:
        for line in f:
            if(line.startswith("cpu ")):
                    line=line.strip()
                    fields=line.split(" ")
                    temp_array.append(fields)
                    #print(fields)
        return temp_array


def extract_total_app_energy(path):
    traces = np.sort([f for f in os.listdir(path) if 'trace' in f and 'clean' not in f])
    #Each Iteration will be treated differently ... so stay tuned!
    total=0.0
    for trace in traces:
        lines = kutils.read_csv_as_array(path+"/"+trace)
        lines = lines[1:-1]
        for line in lines:
            if(float(line[4]) < 0):
                print(path)
                print(trace)
                sys.exit()
            total+= float(line[4])


    return total


def extract_total_app_jiffies(path):
    threads = np.sort([f for f in os.listdir(path) if 'thread' in f and 'clean' not in f])
    #Each Iteration will be treated differently ... so stay tuned!
    total=0
    for thread in threads:
        thread_jiffs = {}
        lines = kutils.read_csv_as_array(path+"/"+thread)
        lines = lines[1:-1]
        lines.reverse()
        for line in lines:
            thread_name = "P" +  line[3]
            if not(thread_name in thread_jiffs):
               thread_jiffs[thread_name] = int(line[5]) + int(line[6])

        durations = thread_jiffs.values()
        iteration_duration = sum(durations)
        total = total + iteration_duration
    

    return total


def extract_total_sys_jiffies(path):
    jiff_iters = np.sort([f for f in os.listdir(path) if 'jiffies' in f and 'clean' not in f])
    #first_iter_jiff = read_jiffies_as_array(path+"/start")
    #last_iter_jiff  = read_jiffies_as_array(path+"/end")
    print("Extracting sys jiffies from %s" %(path))
    first_iter_jiff = read_jiffies_as_array(path+"/"+jiff_iters[0])
    last_iter_jiff  = read_jiffies_as_array(path+"/"+jiff_iters[-1])
    start_jiffs     =  sum([int(first_iter_jiff[0][i]) for i in [2,3,4,7,8]]) 
    end_jiffs       =  sum([int(last_iter_jiff[-1][i]) for i in [2,3,4,7,8]])
    return (start_jiffs,end_jiffs)

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-jarname', action="store", default="chappie.dacapo-9.12-bach", dest="jarname")
parser.add_argument('-path', action="store", default="./dacapo_x_x", dest="path")
parser.add_argument('-destination', action="store", default="dacapo_app.data", dest="destination")
args = parser.parse_args()

path = os.path.expanduser(args.path)
destination = os.path.expanduser(args.destination)
jarname = args.jarname
if not os.path.exists(destination):
    os.makedirs(destination)



experiments = np.sort([f for f in os.listdir(path)])
csv_data=[]

for experiment in experiments:
    ##Calculate the energy contribution of each experiment
    exp_benchmarks_path = path+"/"+experiment
    print("Processing experiment %s" %(exp_benchmarks_path))
    exp_benchmarks = np.sort([f for f in os.listdir(exp_benchmarks_path)])
    
    #experiment_readings = {}
    bench_names=[]
    bench_jiffs=[]
    bench_eng=[]

    longest_bench=0
    total_apps=0
    least_start=-1
    largest_end=0
    total_sys=0
    max_energy=0.0
    for exp_benchmark in exp_benchmarks: 
            if (exp_benchmark=="start" or exp_benchmark=="end"):
                continue

            print("Processing %s" %(exp_benchmark))
            data_file_paths = exp_benchmarks_path + "/" + exp_benchmark + "/" + jarname
            total_sys_jiffies     = extract_total_sys_jiffies(data_file_paths)
            total_app_jiffies     = extract_total_app_jiffies(data_file_paths)
            total_app_energy      = extract_total_app_energy(data_file_paths)
       
            bench_names.append(exp_benchmark)
            bench_jiffs.append(total_app_jiffies)

            if(total_app_energy > max_energy):
                max_energy = total_app_energy

            total_apps=total_apps+total_app_jiffies
            if(total_app_jiffies>longest_bench):
                longest_bench = total_app_jiffies

            if(total_sys_jiffies[1]> largest_end):
                largest_end = total_sys_jiffies[1]

            if(least_start==-1):
                least_start = total_sys_jiffies[0]
            else:
                if(total_sys_jiffies<least_start):
                    least_start=total_sys_jiffies[0]
    
    sys_jiff = largest_end-least_start
    if(sys_jiff > total_apps):
        ##Calculate other
        ##Calculate energy for each benchmark
        i = 0
        other_jiff=sys_jiff
        print(max_energy)
        while i < len(bench_names):
            bench_eng.append((float(bench_jiffs[i])/float(sys_jiff))*max_energy)            
            other_jiff=other_jiff - bench_jiffs[i]
            i = i + 1
        
        bench_names.append("other")
        bench_eng.append((float(other_jiff)/float(sys_jiff))*max_energy)

    cvs_entry=[]
    cvs_entry.append(str(len(bench_names)+1))
   
    i=0
    while i < len(bench_names):
        cvs_entry.append(bench_names[i])
        cvs_entry.append(str(bench_eng[i]))
        i=i+1
    
    csv_data.append(",".join(cvs_entry))



print("+++++++++++")
kutils.write_file("eng.data",csv_data)
    
            

            



    

quit()

#traces = np.sort([f for f in os.listdir(path) if 'trace' in f])
#threads = np.sort([f for f in os.listdir(path) if 'thread' in f])
# jiffies = np.sort([f for f in os.listdir(path) if 'thread' in f])
#
# jiffies = np.sort([f for f in os.listdir(path) if 'jiffies' in f and 'clean' not in f])
#
# if len(jiffies) > 0:
#    jiffy_dfs = []
#
#    for jiffy in jiffies:
#       print(jiffy)
#
#       df = pd.read_csv(os.path.join(path, jiffy), header = None)
#
#       # separate each line into the desired pieces
#       df = df[df[0].str.contains('cpu')]
#       df = df[0].str.replace('  ', ' ').str.split(' ').map(lambda x: [x[i] for i in [0, 1, 3]]).values.tolist()
#       df = pd.DataFrame(df, columns = ['core', 'psu_jiffies', 'psk_jiffies'], dtype = int)
#
#       # add the chappie values
#       df['core'] = df['core'].str.replace('cpu', '0').astype(int)
#       df['epoch'] = [n for N in [[i] * 41 for i in range(len(df)//41)] for n in N]
#       df['socket'] = df['core'].map(lambda c: 2 if c >= 20 else 1)
#       df = df.drop_duplicates(subset = ['core', 'epoch'], keep = 'last')
#
#       df = df.groupby(['epoch', 'socket'])['psu_jiffies', 'psk_jiffies'].sum().reset_index()
#
#       df = df[['epoch', 'socket', 'psu_jiffies', 'psk_jiffies']]
#
#       jiffy_dfs.append(df)
#       df = df.to_csv(os.path.join(destination, jiffy), index = False)
# else:
#    jiffy_df = [None] * len(traces)

# for i, (thread, trace, jiffy) in enumerate(zip(threads, traces, jiffy_dfs)):
for i, (thread, trace) in enumerate(zip(threads, traces)):
   print('{}, {}'.format(thread, trace))
   trace = pd.read_csv(os.path.join(path, trace))

   trace = trace[['epoch','socket', 'package', 'dram']]
   epoch = trace['epoch']
   socket = trace['socket']

   trace = trace.groupby('socket').diff().fillna(0)
   trace['epoch'] = epoch
   trace['socket'] = socket

   trace.to_csv(os.path.join(destination, 'chappie.trace.{}.csv'.format(i)), index = False)

   thread = pd.read_csv(os.path.join(path, thread))

   thread['socket'] = thread['core'].transform(lambda x: 2 if x > 19 else 1 if x > -1 else -1)

   # print(thread.head(10))

   # thread = thread[thread['state'] != 0]
   # thread = thread[thread['socket'] == thread['socket']]
   # thread['stack'] = thread['stack'].fillna('end')
   # thread = thread[thread['stack'] != 'end']

   # if jiffy is not None:
   #    jiffies = pd.merge(thread, jiffy, on = ['epoch', 'socket'], how = 'left')
   #    jiffies = jiffies.groupby('thread')[['epoch', 'u_jiffies', 'psu_jiffies', 'k_jiffies', 'psk_jiffies']].diff()
   #    jiffies[['u_jiffies', 'k_jiffies']] = jiffies[['u_jiffies', 'k_jiffies']].fillna(0)
   #    jiffies[['psu_jiffies', 'psk_jiffies']] = jiffies[['psu_jiffies', 'psk_jiffies']].fillna(1)
   #
   #    jiffies['u_active'] = jiffies['u_jiffies'] / jiffies['psu_jiffies']
   #    jiffies['k_active'] = jiffies['k_jiffies'] / jiffies['psk_jiffies']
   #    jiffies[['u_active', 'k_active']] = jiffies[['u_active', 'k_active']].replace(np.inf, 0)
   #
   #    thread['u_active'] = jiffies['u_active']
   #    thread['k_active'] = jiffies['k_active']
   #
   #    t_active = thread.groupby(['epoch', 'socket'])[['u_active', 'k_active']].sum()
   #    t_active['u_active'] = [state if state > 0 else 1 for state in t_active['u_active']]
   #    t_active['k_active'] = [state if state > 0 else 1 for state in t_active['k_active']]
   #
   #    jiffies = pd.merge(thread, t_active, on = ['epoch', 'socket'], suffixes = ('', '_t'))
   #
   #    jiffies['u_active'] /= jiffies['u_active_t']
   #    jiffies['k_active'] /= jiffies['k_active_t']
   #
   #    jiffies = pd.merge(jiffies, trace, on = ['epoch', 'socket'])
   #
   #    jiffies['u_package'] = jiffies['u_active'] * jiffies['package']
   #    jiffies['u_dram'] = jiffies['u_active'] * jiffies['dram']
   #
   #    thread = thread[['epoch', 'thread', 'core', 'socket', 'state', 'u_active', 'k_active']]

   unmappables = thread[thread['socket'] == -1]
   unmappables['state'] = 0.5

   unmappables['socket'] = 1

   unmappables2 = unmappables.copy()
   unmappables2['socket'] = 2

   activity = pd.concat([thread[thread['socket'] != -1], unmappables, unmappables2]).sort_values(by = 'epoch')
   activity = activity.groupby(['socket', 'epoch'])[['core', 'state']].sum().reset_index().rename(columns = {'state': 'count'})
   activity = pd.merge(trace, activity, on = ['epoch', 'socket'])
   activity['package'] /= activity['count']
   activity['dram'] /= activity['count']

   thread = pd.merge(thread, activity, on = ['epoch', 'socket']).drop(columns = ['count', 'state'])
   thread['core'] = thread['core_x']

   thread[['epoch', 'thread', 'core', 'socket', 'package', 'dram', 'stack']].to_csv('{}/result_{}.csv'.format(destination, i), index = False)
