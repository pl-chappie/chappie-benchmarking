#!/bin/bash

# usage: Invoke with old to skip the method alignment, invoke with new to perform it:
#   ./analyze_dacapo.sh old
#   ./analyze_dacapo.sh new

#if [[ $# -ne 1 ]]; then
#  echo "usage: $0 [CHAPPIE VERSION]"
#  exit
#fi

p="./chappie.dacapo"
out="./processed_data"

if [[ $# -eq 3 ]]; then
	p="$2"
  out="$3"
fi

mkdir $out

echo "Using Path $p, outdir $out"
experiments=$(ls $p)
echo $experiments
for f in $experiments;
do
	path="$p/$f"
	benchs=$(ls $path)
	experiment_name=$(echo "$path" | rev | cut -d / -f 1|rev)
	mkdir $out/$experiment_name
	for benchmark in $benchs;
	do
		if [ -d $path/${benchmark}/chappie.dacapo-9.12-bach ];
		then
			mv $path/${benchmark}/chappie.dacapo-9.12-bach/* $path/${benchmark}/
		fi

		if [[ $1 = "new" ]]; then
			echo  "Benchmark:$path/${benchmark}"		
			nlogs=`find $path/${benchmark} -maxdepth 1 -type f -name '*.thread.*.csv' | wc -l`
			nlogs=$((nlogs-1))
			d=$path/${benchmark}
			echo "Processing $benchmark"
			echo $nlogs		
			for i in `seq 0 $nlogs`; do
				./opt-align.rb -c $d/chappie.thread.$i.csv -h $d/log.$i.hpl > $d/aligned.$i.csv
			done
			for i in `seq 0 $nlogs`; do
			 	 mv $d/chappie.thread.$i.csv $d/saved.$i.csv
			 	 mv $d/aligned.$i.csv $d/chappie.thread.$i.csv
			done
		fi
		/usr/bin/python3.5 data_processing.py -path="$path/${benchmark}" -benchmark=${benchmark} -destination="$out/${benchmark}"
				#Method Attribution script
		/usr/bin/python3.5 method-attribution.py -path="$path/${benchmark}" -benchmark=${benchmark} -destination="$out/${benchmark}"
		
		mv $out/$benchmark $out/$experiment_name		 

	done
done







