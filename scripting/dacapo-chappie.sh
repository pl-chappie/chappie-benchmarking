#!/bin/bash

rm -rf chappie.dacapo
mkdir chappie.dacapo

# benchmarks="avrora batik eclipse h2 jython luindex lusearch pmd sunflow tomcat tradebeans tradesoap xalan"
#benchmarks="avrora h2 jython luindex lusearch pmd sunflow xalan"
benchmarks="sunflow"

# ./run.sh ../../chappiebench/dacapobench/benchmarks/dacapo.jar "" Harness sunflow
for benchmark in $benchmarks; do
  start_time=$(date +%s)
  /home/rsaxena3/work/chappie/run/run.sh ../../chappiebench/dacapo/dacapo-9.12-bach.jar "" Harness $benchmark
  stop_time=$(date +%s)
  echo $((stop_time - start_time))>> /tmp/sunflow_mode0

  mkdir $benchmark
  mv chappie.dacapo-9.12-bach/*.csv $benchmark
  mv $benchmark chappie.dacapo/$benchmark
done

rm scratch -rf
