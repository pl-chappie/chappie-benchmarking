# Analyzing data

We have scripts to speed up the analysis of benchmarks, namely, cold-dacapo.sh and analyze_dacapo.sh. The first script will bootstrap chappie to run the dacapo benchmark suite. The second will take the produced set of files and perform chappie analysis. Please see the scripts to select specific benchmarks to run and analyze. In addition, a graphing scripting, graph_dacapo.sh takes the processed data from analyze_dacapo.sh (we include our processed data, see below) and produces a set of figures.

# Data

Our processed data collected from benchmarks can be found under /processed_data. Each benchmark will have a folder with data files detailing the attributed threads (result_i.csv), attributed components (component_i.csv), method ranking (filtered_attribution_i.csv). Similarly, data for co-application runs and runs with foreign loads can be found under ./scripting/procssed_data and ./scripting/foreign_data, where each directory indicates the combination of applications used.

/processed_data/graphs contains figures produced from the processed data for all benchmarks.
