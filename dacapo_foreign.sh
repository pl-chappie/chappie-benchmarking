#!/bin/bash

PARSECBIN=/home/acanino1/Projects/parsec-3.0/bin

export ITERS=1
export MODE=VM_SAMPLE
export MEMORY=false
export POLLING=4
export CORE_RATE=10
export READ_JIFFIES=TRUE
iters=9

echo "Running Benchmarks $1 - Ferret"

instance_a="$1"

if [ -d "${instance_a}" ]
then
	rm -rf "${instance_a}"
fi

mkdir "${instance_a}"

echo "Execution $0 ..."
file_dir=`dirname "$0"`

if [ -d /tmp/i1 ] 
then
	rm -rf /tmp/i1
fi

mkdir /tmp/i1

for i in $(seq 0 $iters); do
	cd "${instance_a}"
	../../chappie/run/run.sh ../dacapo-9.12-bach.jar "" Harness "-no-validation $1" &
	first_id=$!
	cd ../
  
  # Kickoff a parsec run
  $PARSECBIN/parsecmgmt -a run -p ferret -i simlarge
	second_id=$!
	
	tail --pid=${first_id} -f /dev/null
	tail --pid=${second_id} -f /dev/null

	cd ${instance_a}
	cd chappie.dacapo-9.12-bach
	ls
	index=$((i))	
	for file in *.*.0.csv; do
    mv $file /tmp/i1/"${file%.0.csv}.${index}.csv"
	done

	for file in *.*.0.txt; do
    mv $file /tmp/i1/"${file%.0.txt}.${index}.csv"
	done

  mv log.hpl "/tmp/i1/log.${index}.hpl"	
	cd ../../
done

if [ -d "$1_ferret" ]
then
	rm -r "$1_ferret"
fi
mkdir $1_ferret


cp  /tmp/i1/* "${instance_a}/chappie.dacapo-9.12-bach"
cp  /tmp/i1/* "${instance_a}"
cp -r "${instance_a}" "$1_ferret"


