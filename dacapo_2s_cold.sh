#!/bin/bash

export ITERS=1
export MODE=VM_SAMPLE
export MEMORY=false
export POLLING=4
export CORE_RATE=10
export READ_JIFFIES=TRUE
iters=9

echo "Running Benchmarks $1 - $2"

instance_a="$1"
instance_b="$2"

if [ $1 == $2 ]
then
	instance_a="$1_1"
	instance_b="$2_2"
fi


if [ -d "${instance_a}" ]
then
	rm -rf "${instance_a}"
fi


if [ -d "${instance_b}" ]
then
	rm -rf "${instance_b}"
fi

mkdir "${instance_a}"
mkdir "${instance_b}"


echo "Execution $0 ..."
echo "Running $1 $2 simultaneously ..."
file_dir=`dirname "$0"`
#${file_dir}/../chappie/run/run.sh bench_jars/dacapo-9.12-bach.jar "" Harness "-no-validation ${benchmarks[1]}"

if [ -d /tmp/i1 ] 
then
	rm -rf /tmp/i1
	rm -rf /tmp/i2
fi

mkdir /tmp/i1
mkdir /tmp/i2


for i in $(seq 0 $iters); do
	cd "${instance_a}"
	../../chappie/run/run.sh ../dacapo-9.12-bach.jar "" Harness "-no-validation $1" &
	first_id=$!
	cd ../
	cd "${instance_b}"
	../../chappie/run/run.sh ../dacapo-9.12-bach.jar "" Harness "-no-validation $2" &
	second_id=$!
	cd ../
	
	tail --pid=${first_id} -f /dev/null
	tail --pid=${second_id} -f /dev/null

	cd ${instance_a}
	cd chappie.dacapo-9.12-bach
	ls
	index=$((i))	
	for file in *.*.0.csv; do
        	mv $file /tmp/i1/"${file%.0.csv}.${index}.csv"
		#pwd
		#echo "${file}"
	done

	for file in *.*.0.txt; do
        	mv $file /tmp/i1/"${file%.0.txt}.${index}.csv"
		#pwd
		#echo ${file}
	done

   	mv log.hpl "/tmp/i1/log.${index}.hpl"	
	cd ../../

	cd ${instance_b}
	cd chappie.dacapo-9.12-bach
	index=$((i))

	for file in *.*.0.csv; do
		#echo "Moving $file to ${file%.0.csv}.${index}.csv" >> ../../logs
		mv $file /tmp/i2/"${file%.0.csv}.${index}.csv"
		#pwd
		#echo $file
	done

	for file in *.*.0.text; do
		#pwd
		#echo $file
		mv $file /tmp/i2/"${file%.text.csv}.${index}.txt"
	done

   	mv log.hpl "/tmp/i2/log.${index}.hpl"	
	cd ../../
done

if [ -d "$1_$2" ]
then
	rm -r "$1_$2"
fi
mkdir $1_$2


cp  /tmp/i1/* "${instance_a}/chappie.dacapo-9.12-bach"
cp  /tmp/i2/* "${instance_b}/chappie.dacapo-9.12-bach"
cp  /tmp/i1/* "${instance_a}"
cp  /tmp/i2/* "${instance_b}"
cp -r "${instance_a}" "$1_$2"
cp -r "${instance_b}" "$1_$2"


