echo "Analyzing Graphchi"

benchmarks=(
	ConnectedComponents
	Pagerank
)

for benchmark in "${benchmarks[@]}"; do
	echo "Processing $benchmark ..."
	/usr/bin/python3.5 /home/rsaxena3/work/chappie-graph/data_processing.py -path="./chappie.graphchi/${benchmark}" -benchmark=${benchmark} -destination="processed_data_graphchi/${benchmark}"

	/usr/bin/python3.5 /home/rsaxena3/work/chappie-graph/method-attribution.py -path="./chappie.graphchi/${benchmark}" -benchmark=${benchmark} -destination="processed_data_graphchi/${benchmark}"
done
