
benchmarks=(
  batik
  sunflow 
  avrora 
  eclipse 
  h2 
  jython 
  luindex 
  lusearch 
  pmd 
  tomcat 
  tradebeans 
  tradesoap 
  xalan
)

echo "Dacapo Suit"
echo "++++++++++++++++++++++++++++++++++++++"
echo "${benchmarks[0]} ok"
echo "${benchmarks[1]} ok"
echo "${benchmarks[2]} ok"
echo "${benchmarks[3]} ok"
echo "${benchmarks[4]} ok"
echo "${benchmarks[5]} ok"
echo "${benchmarks[6]} ok"
echo "${benchmarks[7]} ok"
echo "${benchmarks[8]} ok"
echo "${benchmarks[9]} ok"
echo "${benchmarks[10]} ok"
echo "${benchmarks[11]} bad"
echo "${benchmarks[12]} ok"


