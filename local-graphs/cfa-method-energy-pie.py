import pandas as pd
import os
import argparse
import re
import matplotlib as mpl

if os.environ.get('DISPLAY', '') == '':
    mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-benchmark', action="store", default="sunflow", dest="benchmark")
parser.add_argument('-path', action="store", default="~/Desktop/sunflow/processed_data", dest="path")
parser.add_argument('-destination', action="store", default="~/Desktop/sunflow/processed_data/graphs",
                    dest="destination")
args = parser.parse_args()

path = os.path.expanduser(args.path)
benchmark = args.benchmark
destination = os.path.expanduser(args.destination)
if not os.path.exists(destination):
    os.makedirs(destination)
total_energy = 0



cfa_nums = [1,2]

for cfanum in cfa_nums:
    bigData = pd.DataFrame(columns=['method', 'dram1', 'package1', 'dram2', 'package2', 'energy'])
    cfData = pd.DataFrame(columns=['method', 'dram1', 'package1', 'dram2', 'package2', 'energy'])

    total_energy = 0

    numOfFiles = 0
    for entry in os.scandir(path):
        if entry.is_file() and "method_attribution" in entry.name:
            numOfFiles += 1
    for i in range(3, numOfFiles):
        df = pd.read_csv(path + '/method_attribution_' + str(i) + '.csv')
        cfData = cfData.append(df, ignore_index=True)
        cfData = cfData.fillna(value=0)
        data = pd.read_csv(path + '/result_' + str(i) + '.csv')
        data['energy'] = data['dram'] + data['package']
        total_energy += data['energy'].sum()

    cfData['method'] = cfData['method'].apply(lambda x: re.sub(r"\(+.*\)+", "", x))
    cfData['method'] = cfData['method'].apply(lambda x: ((x.split("."))[len(x.split("."))-2]) + '.'+ ((x.split("."))[len(x.split("."))-1]))

    cfData = cfData.groupby(['method'])['dram1', 'package1', 'dram2', 'package2', 'energy'] \
        .sum(numeric_only=False)
    cfData.reset_index(inplace=True)
    cfData = cfData.sort_values("energy",ascending=False).head(3)

    numOfFiles = 0
    for entry in os.scandir(path):
        if entry.is_file() and "{}CFA_attribution".format(cfanum) in entry.name:
            numOfFiles += 1
    for i in range(3, numOfFiles):
        df = pd.read_csv(path + '/{}CFA_attribution_'.format(cfanum) + str(i) + '.csv')
        bigData = bigData.append(df, ignore_index=True)
        bigData = bigData.fillna(value=0)
        data = pd.read_csv(path + '/result_' + str(i) + '.csv')
        data['energy'] = data['dram'] + data['package']
        total_energy += data['energy'].sum()

    bigData = bigData.groupby(['method'])['dram1', 'package1', 'dram2', 'package2', 'energy'] \
        .sum(numeric_only=False)
    bigData.reset_index(inplace=True)
    #with PdfPages(destination + '/cfa-method-energy-proportion-' + benchmark + '.pdf') as pdf:
    #    newDf = pd.DataFrame(columns=['method', 'percent'])
    #    count = 0
    #    for method in bigData['method']:
    #        energy = float(bigData.loc[bigData['method'] == method]['energy'])
    #        percent = energy / total_energy * 100
    #        newDf.loc[count] = [method, percent]
    #        count += 1

    mpl.rcParams.update({'font.size': 8})

    with PdfPages(destination + '/{}cfa-method-energy-pie-'.format(cfanum) + benchmark + '.pdf') as pdf:
        topAnalysis = np.array(cfData['method'])
        for md in topAnalysis:
            print(md)
            callingcontexts = bigData[bigData['method'].str.startswith(md)]
            print(callingcontexts)
            ax = callingcontexts.plot(title=md, kind='pie', labels=None, y=['energy'],figsize=(6, 6))
            ax.legend(labels=callingcontexts['method'])

            #plt.xlabel('Percent Consumption of Energy')
            plt.tight_layout()
            pdf.savefig()
            #plt.show()
            plt.close()

