import os
import argparse
import pandas as pd
import numpy as np
import matplotlib as mpl
if os.environ.get('DISPLAY','') == '':
  print('no display found. Using non-interactive Agg backend')
  mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-benchmark', action="store", default="graphchi", dest="benchmark")
parser.add_argument('-path', action="store", default="~/Desktop/", dest="path")
parser.add_argument('-destination', action="store", default="./benchmark_graphs", dest="destination")
args = parser.parse_args()

path = args.path
benchmark = args.benchmark
destination = args.destination
#path = '~/Desktop/'
#benchmark = 'graphchi'
if not os.path.exists(destination):
    os.makedirs(destination)

numOfFiles = 0
for entry in os.scandir(path):
    if entry.is_file() and "method_attribution" in entry.name:
        numOfFiles += 1

with PdfPages(destination+'/thread-movement-' + benchmark + '.pdf') as pdf:
    firstPage = plt.figure(figsize=(70,50))
    firstPage.clf()
    txt = 'thread-movement-' + benchmark
    firstPage.text(0.5,0.5,txt, transform=firstPage.transFigure, size=40, ha="center")
    pdf.savefig()
    plt.close()

    for i in range(numOfFiles):
        df = pd.read_csv(path + '/result_'+str(i)+'.csv')
        for choice in ['Reference Handler', 'Chaperone', 'main', 'Signal Dispatcher', 'Finalizer']:
            df = df[df['thread'] != choice]
        if df.empty:
          print('Dataframe is empty after removing system threads in result_{}.csv. Unable to plot'.format(i))
          continue
        threads = list(df['thread'].unique()[:5])
        df = df[df['thread'].isin(threads)]
        df_pivot = df.pivot_table(index='epoch', columns='thread', values='core')
        df_pivot.fillna(value=0, inplace=True)

        x = np.array(df_pivot.index)
        #df_pivot.to_csv('~/Desktop/thread-movement.csv')

        plotList = []
        labels = []
        for p in range(len(df_pivot.columns)):
          plotList.append(tuple(np.array(df_pivot.iloc[:, p])))
          labels.append(df_pivot.columns[p]) 

        colors = ['skyblue', 'green', 'red', 'violet', 'orange']
        index = 0
        plt.figure(figsize=(70, 50))
        for plotTuple in plotList:
          plt.plot(x, plotTuple, data=df, marker='o', markerfacecolor=colors[index], markersize=8, color=colors[index], linewidth=1)
          index+=1

        plt.yticks(np.arange(0, 40, 1), fontsize=40)
        plt.xticks(fontsize=40)
        plt.xlabel('Epoch: 1 unit as 4ms', fontsize=40)
        plt.ylabel('Core', fontsize=40)

        plt.legend(labels, prop={'size': 40})
        pdf.savefig()
        plt.show()
        plt.close()
