import os
import argparse
import pandas as pd
import numpy as np
import matplotlib as mpl
if os.environ.get('DISPLAY','') == '':
    print('no display found. Using non-interactive Agg backend')
    mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-benchmark', action="store", default="graphchi", dest="benchmark")
parser.add_argument('-path', action="store", default="~/Desktop/", dest="path")
parser.add_argument('-destination', action="store", default="./benchmark_graphs", dest="destination")
args = parser.parse_args()

path = args.path
benchmark = args.benchmark
destination = args.destination
if not os.path.exists(destination):
    os.makedirs(destination)

numOfFiles = 0
for entry in os.scandir(path):
    if entry.is_file() and "method_attribution" in entry.name:
        numOfFiles += 1

with PdfPages(destination+'/physical-attribution-'+benchmark+'.pdf') as pdf:
    firstPage = plt.figure(figsize=(30,20))
    firstPage.clf()
    txt = 'physical-attribution-' + benchmark
    firstPage.text(0.5,0.5,txt, transform=firstPage.transFigure, size=40, ha="center")
    pdf.savefig()
    plt.close()

    for i in range(numOfFiles):
        df = pd.read_csv(path+'/system-application_'+str(i)+'.csv')
        df = df[['time', 'sock1_app_package', 'sock1_app_dram', 'sock2_app_package', 'sock2_app_dram']]
        if len(df)<25:
          split = np.array_split(df, 1)
        else:
          split = np.array_split(df, len(df)/25)
        newDf = pd.DataFrame(columns=['time', 'package_sock1', 'package_sock2', 'dram_sock1', 'dram_sock2'])
        for frame in range(len(split)):
            ind = split[frame].index.max()
            time = split[frame].loc[ind]['time']
            package_sock1 = split[frame]['sock1_app_package'].sum()
            package_sock2 = split[frame]['sock2_app_package'].sum()
            dram_sock1 = split[frame]['sock1_app_dram'].sum()
            dram_sock2 = split[frame]['sock2_app_dram'].sum()
            newDf.loc[frame] = [time, package_sock1, package_sock2, dram_sock1, dram_sock2]

        #newDf['time']= newDf['time'].astype(str)
        newDf.set_index('time', inplace=True)
        plot_params = ['package_sock1', 'package_sock2', 'dram_sock1', 'dram_sock2']
        newDf.loc[:, plot_params].plot.bar(stacked=True, figsize=(30, 20))
        #locator = matplotlib.ticker.MultipleLocator()
        #plt.gca().xaxis.set_major_locator(locator)
        #formatter = matplotlib.ticker.StrMethodFormatter("{x:.0f}")
        #plt.gca().xaxis.set_major_formatter(formatter)
        plt.legend(loc='best')
        plt.xlabel('Elapsed Time (50 ms as 1 unit)', fontsize=15)
        plt.ylabel('Energy (J)', fontsize=15)

        pdf.savefig()
        plt.close()
