#!/usr/bin/python3

import numpy as np
import pandas as pd
import argparse
import os
import re

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-benchmark', action="store", default="sunflow", dest="benchmark")
parser.add_argument('-path', action="store", default="~/Desktop/sunflow", dest="path")
parser.add_argument('-destination', action="store", default="~/Desktop/sunflow/processed_data",
                    dest="destination")
args = parser.parse_args()

path = os.path.expanduser(args.path)
benchmark = args.benchmark
destination = os.path.expanduser(args.destination)
if not os.path.exists(destination):
    os.makedirs(destination)

# path = './chappie.chappie_test'
# destination = './chappie.chappie_test.processed'

threads = np.sort([f for f in os.listdir(destination) if 'result' in f])


def fixstr(x):
    s = re.sub(r"\(+.*\)+", "", x)
    s2 = ((s.split("."))[len(s.split(".")) - 2]) + '.' + ((s.split("."))[len(s.split(".")) - 1])
    return s2


for i, thread in enumerate(threads):
    filename = str(thread)
    print(os.path.join(destination, thread))
    thread = pd.read_csv(os.path.join(destination, thread))

    # Filter out system thread and Chappie from analysis
    thread = thread[~thread['thread'].str.contains('Chaperone')]

    thread = thread.groupby(['stack', 'socket'])[['package', 'dram']].sum().reset_index()
    thread1 = pd.DataFrame(thread)
    thread2 = pd.DataFrame(thread)

    thread['method'] = thread['stack'].str.split(';')
    thread1['method'] = thread1['stack'].str.split(';')
    thread2['method'] = thread2['stack'].str.split(';')
    thread = thread.dropna()
    thread1 = thread1.dropna()
    thread2 = thread2.dropna()

    thread['method'] = thread['method'].map(lambda x: x[0])
    # Extract "java." methods
    javathread = thread[thread['method'].str.contains('java\.') & ~thread['method'].isin(['end', ''])]
    javathread['method'] = javathread['method'].apply(lambda x: fixstr(x))

    # Perform further operations on threads
    thread['method'] = thread['method'].apply(lambda x: re.sub(r"\(+.*\)+", "", x))
    thread['method'] = thread['method'].apply(
        lambda x: ((x.split("."))[len(x.split(".")) - 2]) + '.' + ((x.split("."))[len(x.split(".")) - 1]))

    # thread1['method'] = thread1['method'].map(lambda x: fixstr(x[0]) + "|" + fixstr(x[1]))
    thread1['method'] = thread1['method'].map(
        lambda x: (fixstr(x[0]) + "|" + fixstr(x[1])) if len(x) > 1 else fixstr(x[0]))

    thread2['method'] = thread2['method'].map(
        lambda x: (fixstr(x[0]) + "|" + fixstr(x[1]) + "|" + fixstr(x[2])) if len(x) > 2 else fixstr(x[0]))

    thread = thread[~thread['stack'].str.startswith('java.') & ~thread['stack'].isin(['end', ''])]
    thread1 = thread1[~thread1['stack'].str.startswith('java.') & ~thread1['stack'].isin(['end', ''])]
    thread2 = thread2[~thread2['stack'].str.startswith('java.') & ~thread2['stack'].isin(['end', ''])]

    hotmethod = thread.groupby(['method']).size().reset_index(name='hits')
    hotmethod1 = thread1.groupby(['method']).size().reset_index(name='hits')
    hotmethod2 = thread2.groupby(['method']).size().reset_index(name='hits')
    javahotmethods = javathread.groupby(['method']).size().reset_index(name='hits')

    thread = thread.groupby(['method', 'socket'])[['package', 'dram']].sum().reset_index()
    thread1 = thread1.groupby(['method', 'socket'])[['package', 'dram']].sum().reset_index()
    thread2 = thread2.groupby(['method', 'socket'])[['package', 'dram']].sum().reset_index()
    javathread = javathread.groupby(['method', 'socket'])[['package', 'dram']].sum().reset_index()

    package = thread.reset_index().pivot(index='method', columns='socket', values='package').fillna(0).reset_index().rename(
        columns={1: 'package1', 2: 'package2'})
    package1 = thread1.reset_index().pivot(index='method', columns='socket', values='package').fillna(
        0).reset_index().rename(columns={1: 'package1', 2: 'package2'})
    package2 = thread2.reset_index().pivot(index='method', columns='socket', values='package').fillna(
        0).reset_index().rename(columns={1: 'package1', 2: 'package2'})
    javapackage = javathread.reset_index().pivot(index='method', columns='socket', values='package').fillna(
        0).reset_index().rename(columns={1: 'package1', 2: 'package2'})

    dram = thread.reset_index().pivot(index='method', columns='socket', values='dram').fillna(0).reset_index().rename(
        columns={1: 'dram1', 2: 'dram2'})
    dram1 = thread1.reset_index().pivot(index='method', columns='socket', values='dram').fillna(0).reset_index().rename(
        columns={1: 'dram1', 2: 'dram2'})
    dram2 = thread2.reset_index().pivot(index='method', columns='socket', values='dram').fillna(0).reset_index().rename(
        columns={1: 'dram1', 2: 'dram2'})
    javadram = javathread.reset_index().pivot(index='method', columns='socket', values='dram').fillna(
        0).reset_index().rename(columns={1: 'dram1', 2: 'dram2'})

    thread = pd.merge(package, dram, on='method')
    thread1 = pd.merge(package1, dram1, on='method')
    thread2 = pd.merge(package2, dram2, on='method')
    javathread = pd.merge(javapackage, javadram, on='method')

    thread['energy'] = thread[[col for col in thread.columns if '1' in col or '2' in col]].sum(axis=1)
    thread1['energy'] = thread1[[col for col in thread1.columns if '1' in col or '2' in col]].sum(axis=1)
    thread2['energy'] = thread2[[col for col in thread2.columns if '1' in col or '2' in col]].sum(axis=1)
    javathread['energy'] = javathread[[col for col in javathread.columns if '1' in col or '2' in col]].sum(axis=1)

    thread.to_csv('{}/method_attribution_{}.csv'.format(destination, i), index=False)
    thread1.to_csv('{}/1CFA_attribution_{}.csv'.format(destination, i), index=False)
    thread2.to_csv('{}/2CFA_attribution_{}.csv'.format(destination, i), index=False)
    javathread.to_csv('{}/javathread_attribution_{}.csv'.format(destination, i), index=False)

    top_10 = thread.sort_values('energy', ascending=False).head(10)
    top_10_cfa = thread1.sort_values('energy', ascending=False).head(10)
    top_10_java = javathread.sort_values('energy', ascending=False).head(10)

    top_10.to_csv(destination + '/top10methods_' + str(i) + '.csv')
    top_10_cfa.to_csv(destination + '/1CFA_top10_' + str(i) + '.csv')
    top_10_java.to_csv(destination + '/java_top10_' + str(i) + '.csv')

    hotmethod.to_csv('{}/method_hotness_{}.csv'.format(destination, i), index=False)
    hotmethod1.to_csv('{}/1CFA_hotness_{}.csv'.format(destination, i), index=False)
    javahotmethods.to_csv('{}/java_hotness_{}.csv'.format(destination, i), index=False)

    top_10 = hotmethod.sort_values("hits", ascending=False).head(10)
    top_10_cfa = hotmethod1.sort_values("hits", ascending=False).head(10)
    top_10_java = javahotmethods.sort_values("hits", ascending=False).head(10)

    top_10.to_csv(destination + '/top10methods_hits_' + str(i) + '.csv')
    top_10_cfa.to_csv(destination + '/1CFA_top10methods_hits_' + str(i) + '.csv')
    top_10_java.to_csv(destination + '/java_top10methods_hits_' + str(i) + '.csv')

    # Deep attrib
    thread = pd.read_csv(os.path.join(destination, filename))
    thread = thread.groupby(['stack', 'socket'])[['package', 'dram']].sum().reset_index()
    thread['method'] = thread['stack'].str.split(';')
    thread = thread.dropna()
    thread['method'] = thread['method'].apply(
        lambda methodlist: list(filter(lambda s: s.startswith("java.") == False, methodlist))[0])
    thread = thread.groupby(['method', 'socket'])[['package', 'dram']].sum().reset_index()

    package = thread.reset_index().pivot(index='method', columns='socket', values='package').fillna(
        0).reset_index().rename(columns={1: 'package1', 2: 'package2'})
    dram = thread.reset_index().pivot(index='method', columns='socket', values='dram').fillna(0).reset_index().rename(
        columns={1: 'dram1', 2: 'dram2'})

    thread = pd.merge(package, dram, on='method')
    thread['energy'] = thread[[col for col in thread.columns if '1' in col or '2' in col]].sum(axis=1)

    thread.to_csv('{}/method_deep_attribution_{}.csv'.format(destination, i), index=False)

    top_10 = thread.sort_values('energy', ascending=False).head(10)
    top_10.to_csv(destination + '/top10methods_deep' + str(i) + '.csv')

    hotmethod.to_csv('{}/method_deep_hotness_{}.csv'.format(destination, i), index=False)
    top_10 = hotmethod.sort_values("hits", ascending=False).head(10)
    top_10.to_csv(destination + '/deep_top10methods_hits_' + str(i) + '.csv')
