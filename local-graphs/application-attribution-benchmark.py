import os
import argparse
import math
import pandas as pd
import numpy as np
import matplotlib as mpl
if os.environ.get('DISPLAY','') == '':
    print('no display found. Using non-interactive Agg backend')
    mpl.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-benchmark', action="store", default="graphchi", dest="benchmark")
parser.add_argument('-path', action="store", default="~/Desktop/", dest="path")
parser.add_argument('-destination', action="store", default="./benchmark_graphs", dest="destination")
args = parser.parse_args()


path = os.path.expanduser(args.path)
benchmark = args.benchmark
destination = os.path.expanduser(args.destination)

#path = '~/Desktop/'
#benchmark = 'graphchi'
if not os.path.exists(destination):
    os.makedirs(destination)

numOfFiles = 0
for entry in os.scandir(path):
    if entry.is_file() and "result" in entry.name:
        numOfFiles += 1

with PdfPages(destination+'/application-attribution-'+benchmark+'.pdf') as pdf:
    firstPage = plt.figure(figsize=(150,50))
    firstPage.clf()
    txt = 'application-attribution-'+benchmark
    firstPage.text(0.5,0.5,txt, transform=firstPage.transFigure, size=45, ha="center")
    pdf.savefig()
    plt.close()
    for i in range(numOfFiles):
        df = pd.read_csv(path + '/result_'+str(i)+'.csv')
        for choice in ['Reference Handler', 'Chaperone', 'main', 'Signal Dispatcher', 'Finalizer']:
            df = df[df['thread'] != choice]

        df = df.loc[df['core'] != -1]
        #Check to see if dataframe is empty. Since it can't process further, continue
        if(df.empty):
          print("Empty DataFrame after removing system threads. Can't plot graph")
          continue
        df['energy'] = df['package'] + df['dram']
        df = df.groupby(['epoch', 'thread'], as_index=False, sort=False)['energy'].sum()
        df_pivot = df.pivot(index='epoch', columns='thread', values='energy')
        df_pivot = df_pivot.fillna(0)

        threads = list(df_pivot.columns)

        # Remove spikes starts

        energy_values = []
        # calculate total thread energies for first 5 timestamps

        for p in range(5 if len(df_pivot) >5 else len(df_pivot)):
            energy_values.append(df_pivot.iloc[p][:].sum())

        # calculate max plotting limit as (min of first 5 timestamps * 100)
        max_lim = min(energy_values) * 100

        # Remove timestamps where total energy > max limit
        for p in range(len(df_pivot)):
            if (df_pivot.iloc[p][:].sum()) > max_lim:
                df_pivot.iloc[p][threads] = 0

        # Remove spike ends

        max_range = max((df_pivot.loc[:, threads].sum(axis=1)))

        fig, ax = plt.subplots(1, 1)
        fig = plt.figure()

        ax = fig.add_subplot(1, 1, 1)
        df_pivot.plot.bar(stacked=True, figsize=(150, 50), ax=ax)   #height 25
        # ax[0].set_aspect('equal')
        plt.legend(loc='best', bbox_to_anchor=(1.07, 1), borderaxespad=0, prop={'size': 40})
        ax.xaxis.set_tick_params(labelsize=40)
        ax.yaxis.set_tick_params(labelsize=40)
        plt.xlabel('Elapsed Time (2 ms as 1 unit)', fontsize=40)
        plt.xticks(rotation=90)

        for index, label in enumerate(ax.xaxis.get_ticklabels()):
            if index % 10 != 0:
                label.set_visible(False)
        plt.ylabel('Energy (J)', fontsize=40)
        plt.yticks(np.arange(0, math.ceil(max_range), 0.5))
        plt.tight_layout()
        pdf.savefig()
        #plt.show()
        plt.close()
