import pandas as pd
import os
import argparse
import re
import matplotlib as mpl

if os.environ.get('DISPLAY', '') == '':
    print('no display found. Using non-interactive Agg backend')
    mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-benchmark', action="store", default="sunflow", dest="benchmark")
parser.add_argument('-path', action="store", default="~/Desktop/sunflow/processed_data", dest="path")
parser.add_argument('-destination', action="store", default="~/Desktop/sunflow/processed_data/graphs",
                    dest="destination")
args = parser.parse_args()

path = os.path.expanduser(args.path)
benchmark = args.benchmark
destination = os.path.expanduser(args.destination)
if not os.path.exists(destination):
    os.makedirs(destination)
bigData = pd.DataFrame(columns=['method', 'dram1', 'package1', 'dram2', 'package2', 'energy'])
total_energy = 0

numOfFiles = 0
for entry in os.scandir(path):
    if entry.is_file() and "method_deep_attribution" in entry.name:
        numOfFiles += 1
for i in range(3, numOfFiles):
    df = pd.read_csv(path + '/method_deep_attribution_' + str(i) + '.csv')
    bigData = bigData.append(df, ignore_index=True)
    bigData = bigData.fillna(value=0)
    data = pd.read_csv(path + '/result_' + str(i) + '.csv')
    data['energy'] = data['dram'] + data['package']
    total_energy += data['energy'].sum()

bigData['method'] = bigData['method'].apply(lambda x: re.sub(r"\(+.*\)+", "", x))
bigData['method'] = bigData['method'].apply(lambda x: ((x.split("."))[len(x.split("."))-2]) + '.'+ ((x.split("."))[len(x.split("."))-1]))

bigData = bigData.groupby(['method'])['dram1', 'package1', 'dram2', 'package2', 'energy'] \
    .sum(numeric_only=False)
bigData.reset_index(inplace=True)
with PdfPages(destination + '/method-deep-energy-proportion-' + benchmark + '.pdf') as pdf:
    firstPage = plt.figure(figsize=(15,10))
    firstPage.clf()
    txt = 'method-energy-proportion-' + benchmark
    firstPage.text(0.5,0.5,txt, transform=firstPage.transFigure, size=24, ha="center")
    pdf.savefig()
    plt.close()
    newDf = pd.DataFrame(columns=['method', 'percent'])
    count = 0
    for method in bigData['method']:
        energy = float(bigData.loc[bigData['method'] == method]['energy'])
        percent = energy / total_energy * 100
        newDf.loc[count] = [method, percent]
        count += 1

    # remove Thread.getStackTrace
    newDf = newDf.loc[newDf['method'] != 'Thread.getStackTrace']
    newDf = newDf.sort_values('percent', ascending=False).head(10)
    print(newDf)
    x = np.array(newDf['method'])
    ax = newDf.plot(kind='bar', x='method', y=['percent'], figsize=(15, 10), color='red')
    ax.set_xticklabels(newDf['method'])
    plt.ylabel('Percent Consumption of Energy')
    plt.xticks(rotation=90)
    plt.tight_layout()
    pdf.savefig()
    # plt.show()
    plt.close()
