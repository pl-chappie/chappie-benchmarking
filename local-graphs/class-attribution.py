#!/usr/bin/python3

import numpy as np
import pandas as pd
import argparse
import os
import re

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-benchmark', action="store", default="sunflow", dest="benchmark")
parser.add_argument('-path', action="store", default="~/Desktop/sunflow", dest="path")
parser.add_argument('-destination', action="store", default="~/Desktop/sunflow/processed_data",
                    dest="destination")
args = parser.parse_args()

path = os.path.expanduser(args.path)
benchmark = args.benchmark
destination = os.path.expanduser(args.destination)
if not os.path.exists(destination):
    os.makedirs(destination)

# path = './chappie.chappie_test'
# destination = './chappie.chappie_test.processed'

threads = np.sort([f for f in os.listdir(destination) if 'result' in f])

def fixstr(x):
    s = re.sub(r"\(+.*\)+", "", x)
    return 2

def getclass(x):
    nms = x.split(".")
    nms.pop()
    return ".".join(nms)

for i, thread in enumerate(threads):
    filename = str(thread)
    print(os.path.join(destination, thread))
    thread = pd.read_csv(os.path.join(destination, thread))

    # Filter out system thread and Chappie from analysis
    thread = thread[~thread['thread'].str.contains('Chaperone')]

    thread = thread.groupby(['stack', 'socket'])[['package', 'dram']].sum().reset_index()

    thread['class'] = thread['stack'].str.split(';')
    thread = thread.dropna() 

    thread['class'] = thread['class'].map(lambda x: x[0]) 
    thread = thread[~thread['stack'].str.startswith('java.') & ~thread['stack'].isin(['end', ''])] 
    thread['class'] = thread['class'].map(lambda x: getclass(x)) 


    thread = thread.groupby(['class', 'socket'])[['package', 'dram']].sum().reset_index() 
    package = thread.reset_index().pivot(index = 'class', columns = 'socket', values = 'package').fillna(0).reset_index().rename(columns = {1: 'package1', 2: 'package2'}) 
    dram = thread.reset_index().pivot(index = 'class', columns = 'socket', values = 'dram').fillna(0).reset_index().rename(columns = {1: 'dram1', 2: 'dram2'}) 
    thread = pd.merge(package, dram, on = 'class') 
    thread['energy'] = thread[[col for col in thread.columns if '1' in col or '2' in col]].sum(axis = 1) 
    thread.to_csv('{}/class_attribution_{}.csv'.format(destination, i), index = False) 
    top_10 = thread.sort_values('energy', ascending = False).head(10)
