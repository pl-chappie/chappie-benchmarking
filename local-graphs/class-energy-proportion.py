import pandas as pd
import os
import argparse
import re
import matplotlib as mpl

if os.environ.get('DISPLAY', '') == '':
    mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-benchmark', action="store", default="graphchi", dest="benchmark")
parser.add_argument('-path', action="store", default="~/Desktop/graphchi_logs/processed_data", dest="path")
parser.add_argument('-destination', action="store", default="~/Desktop/graphchi_logs/processed_data/graphs",
                    dest="destination")
args = parser.parse_args()

path = os.path.expanduser(args.path)
benchmark = args.benchmark
destination = os.path.expanduser(args.destination)
if not os.path.exists(destination):
    os.makedirs(destination)
bigData = pd.DataFrame(columns=['class', 'dram1', 'package1', 'dram2', 'package2', 'energy'])
total_energy = 0

numOfFiles = 0
for entry in os.scandir(path):
    if entry.is_file() and "class_attribution" in entry.name:
        numOfFiles += 1
for i in range(3, numOfFiles):
    df = pd.read_csv(path + '/class_attribution_' + str(i) + '.csv')
    bigData = bigData.append(df, ignore_index=True)
    bigData = bigData.fillna(value=0)
    data = pd.read_csv(path + '/result_' + str(i) + '.csv')
    data['energy'] = data['dram'] + data['package']
    total_energy += data['energy'].sum()

bigData['class'] = bigData['class'].apply(lambda x: re.sub(r"\(+.*\)+", "", x))
bigData['class'] = bigData['class'].apply(lambda x: ((x.split("."))[len(x.split("."))-2]) + '.'+ ((x.split("."))[len(x.split("."))-1]))

bigData = bigData.groupby(['class'])['dram1', 'package1', 'dram2', 'package2', 'energy'] \
    .sum(numeric_only=False)
bigData.reset_index(inplace=True)

print(bigData)

with PdfPages(destination + '/class-energy-proportion-' + benchmark + '.pdf') as pdf:
    #firstPage = plt.figure(figsize=(15,10))
    #firstPage.clf()
    #txt = 'method-energy-proportion-' + benchmark
    #firstPage.text(0.5,0.5,txt, transform=firstPage.transFigure, size=24, ha="center")
    #pdf.savefig()
    #plt.close()
    newDf = pd.DataFrame(columns=['class', 'percent'])
    count = 0
    for method in bigData['class']:
        energy = float(bigData.loc[bigData['class'] == method]['energy'])
        percent = energy / total_energy * 100
        newDf.loc[count] = [method, percent]
        count += 1

    # remove Thread.getStackTrace
    newDf = newDf.loc[newDf['class'] != 'Thread.getStackTrace']
    newDf = newDf.sort_values('percent', ascending=False).head(10)
    print(newDf)

    mpl.rcParams.update({'font.size': 8})

    x = np.array(newDf['class'])
    ax = newDf.plot(kind='barh', x='class', y=['percent'], width=0.2 ,figsize=(6, 4), color='red')

    ax.set_yticklabels([])
    ax.invert_yaxis()
    ax.tick_params(
        axis='y',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=False) # labels along the bottom edge are off

    for i, v in enumerate(x):
        ax.text(0, i - .20, str(v), color='black', fontweight='bold')
    

    plt.xlabel('Percent Consumption of Energy')
    plt.tight_layout()
    pdf.savefig()
    # plt.show()
    plt.close()
