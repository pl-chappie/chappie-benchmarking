import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages

path = '~/Desktop/graphchi_logs/'
benchmark = 'graphchi'
with PdfPages(benchmark+'-stacked-thread-vs-energy.pdf') as pdf:
    for i in range(10):
        df = pd.read_csv(path+'chappie.thread.'+str(i)+'.csv')
        for choice in ['Reference Handler', 'Chaperone', 'main', 'Signal Dispatcher', 'Finalizer']:
            df = df[df['thread'] != choice]

        #slice a new dataframe for socket 1 and add energy
        df_socket1 = df.loc[df['core'] < 20]
        df_socket1['energy_sock1'] = df_socket1['package'] + df_socket1['dram']

        #slice a new dataframe for socket 2 and add energy
        df_socket2 = df.loc[df['core'] >= 20]
        df_socket2['energy_sock2'] = df_socket2['package'] + df_socket2['dram']

        '''When you don't want the group column as the index, pandas keeps it in as a column.(Do as_index= False)'''
        df_socket1 = df_socket1.groupby(['thread'], as_index=False, sort=False)['energy_sock1'].sum()
        df_socket2 = df_socket2.groupby(['thread'], as_index=False, sort=False)['energy_sock2'].sum()


        #Merge both dataframes after sum of energy grouped by threads
        result = pd.merge(df_socket1, df_socket2, on='thread', how='outer')
        result = result.fillna(0)
        print(result)

        #Variables for plotting
        sock1 = tuple(result['energy_sock1'])
        sock2 = tuple(result['energy_sock2'])
        ind = np.arange(result['thread'].size)
        width = 0.50

        #Plot graph(Courtesy: https://matplotlib.org/gallery/lines_bars_and_markers/bar_stacked.html)
        plt.figure(figsize=(15,10))
        p1 = plt.bar(ind, sock1, width)
        p2 = plt.bar(ind, sock2, width, bottom=sock1)
        plt.ylabel('Energy (J)')
        plt.title('PER THREAD ENERGY CONSUMPTION FROM chappie.thread.'+str(i)+'.csv')
        plt.xticks(ind, tuple(result['thread']))
        plt.xticks(rotation=80)
        #plt.yticks(np.arange(0, 50, 5))
        plt.legend((p1[0], p2[0]), ('Socket1', 'Socket2'))
        plt.tight_layout()

        pdf.savefig()
        plt.close()

