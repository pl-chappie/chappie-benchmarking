import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import kutils

 # set width of bar
barWidth = 0.1

benchmark_records=kutils.read_csv_as_array("energy.data")
#Each record represents a benchmark run
#The columns are ordered as : benchmark name, instance one energy, instance two energy, other apps

benchmark_names=[]
instance_a=[]
instance_b=[]
others=[]

i=0
while i < len(benchmark_records):
    record = benchmark_records[i]
    benchmark_names.append(record[1]+"-"+record[3])
    instance_a.append(float(record[2]))
    instance_b.append(float(record[4]))
    others.append(float(record[4]))
    i=i+1


barWidth=barWidth+50

# Set position of bar on X axis
r1 = np.arange(len(instance_a))
r2 = [x + barWidth for x in r1]
r3 = [x + barWidth for x in r2]
    
# Make the plot
plt.bar(r1, instance_a, color='#7f6d5f', width=barWidth, edgecolor='white',label="First")
plt.bar(r2, instance_b, color='#557f2d', width=barWidth, edgecolor='white',label="Second")
plt.bar(r3, others,     color='#2d7f5e', width=barWidth, edgecolor='white',label="others")
     
     # Add xticks on the middle of the group bars
plt.xlabel('group', fontweight='bold')
plt.xticks([r + barWidth for r in range(len(instance_a))], benchmark_names)
      
# Create legend & Show graphic
plt.legend()
plt.show()
plt.savefig("energy.png", dpi=150)
#f.savefig("foo.pdf", bbox_inches='tight')
