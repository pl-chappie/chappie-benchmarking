#!/usr/bin/python3

import os
import argparse
import numpy as np
import pandas as pd

parser = argparse.ArgumentParser(description='Argument Parser')
parser.add_argument('-benchmark', action="store", default="sunflow", dest="benchmark")
parser.add_argument('-path', action="store", default="~/Desktop/sunflow", dest="path")
parser.add_argument('-destination', action="store", default="~/Desktop/sunflow/processed_data/", dest="destination")
args = parser.parse_args()

path = os.path.expanduser(args.path)
benchmark = args.benchmark
destination = os.path.expanduser(args.destination)
if not os.path.exists(destination):
    os.makedirs(destination)

# path = './chappie.chappie_test'
# destination = './chappie.chappie_test.processed/'

threads = np.sort([f for f in os.listdir(path) if 'thread' in f])
traces = np.sort([f for f in os.listdir(path) if 'trace' in f])

for i, (thread, trace) in enumerate(zip(threads, traces)):
    print('{}, {}'.format(thread, trace))
    trace = pd.read_csv(os.path.join(path, trace))

    trace = trace[['epoch', 'socket', 'package', 'dram']]
    epoch = trace['epoch']
    socket = trace['socket']

    trace = trace.groupby('socket').diff().fillna(0)
    trace['epoch'] = epoch
    trace['socket'] = socket

    # trace.to_csv('{}/chappie.trace.{}.csv'.format(destination, i), index = False)
    trace.to_csv('{}/trace_{}.csv'.format(destination, i), index=False)

    thread = pd.read_csv(os.path.join(path, thread))

    thread = thread[thread['state'] > 0.0]


    thread['socket'] = thread['core'].transform(lambda x: 2 if x > 19 else 1 if x > -1 else np.nan)
    thread = thread[thread['socket'] == thread['socket']]

    # NOTE FROM ANTHONY: Threads without methods are valid now, since we cannot get a method
    # trace for every thread at every timestamp
    thread = thread[thread['stack'] != 'end']

    count = thread.groupby(['socket', 'epoch']).count().reset_index()[['epoch', 'socket', 'state']].rename(
        columns={'state': 'count'})
    count = pd.merge(trace, count, on=['epoch', 'socket'])
    count['package'] /= count['count']
    count['dram'] /= count['count']

    thread = pd.merge(count, thread, on=['epoch', 'socket']).drop(columns=['count', 'state'])
    # Rearrange columns for readablility
    #thread = thread[['epoch', 'thread', 'core', 'package', 'dram', 'socket', 'bytes', 'stack']]
    thread = thread[['epoch', 'thread', 'core', 'package', 'dram', 'socket', 'stack']]
    thread.to_csv('{}/result_{}.csv'.format(destination, i), index=False)