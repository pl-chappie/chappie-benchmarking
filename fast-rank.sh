#!/bin/bash

benchmarks=(
avrora  
batik
eclipse 
fop
h2 
jython  
luindex 
lusearch 
pmd 
sunflow 
tomcat     
tradebeans 
tradesoap 
xalan
#ALSMatrixFactorization
#ConnectedComponents
#Pagerank
#twitter
)

reference_dir=./processed_data
fast_dir=./fast_rate_data

echo "app,  pearson,  spearman" > ./scripting/fast.rank

for benchmark in "${benchmarks[@]}"; do
  echo $benchmark
  
  echo "$reference_dir/graphs/$benchmark/filtered_attribution_ranking.csv"
  echo "$fast_dir/graphs/$benchmark/filtered_attribution_ranking.csv"

  /usr/bin/python3.5 ./scripting/method-ranking.py -reference="$reference_dir/graphs/$benchmark/filtered_attribution_ranking.csv" -coapp="$fast_dir/graphs/$benchmark/filtered_attribution_ranking.csv" -resultfile="./scripting/fast.rank" -tag="$benchmark"
done
