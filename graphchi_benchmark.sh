#!/bin/bash

rm -rf chappie.graphchi*

mkdir chappie.graphchibench
mkdir chappie.graphchibench.NOP
mkdir chappie.graphchibench.SAMPLE

tmp="graphchi_temp"
datadir="graphchi_data"

benchmarks=(
Pagerank
ConnectedComponents
ALSMatrixFactorization
)

export ITERS=1
export MODE=NOP
export POLLING=4
export CORE_RATE=10
export READ_JIFFIES=true

iter=0
for benchmark in "${benchmarks[@]}"; do
  mkdir chappie.graphchibench/${benchmark}
  for i in $(seq 0 $iter); do
    ../chappie/run/run.sh graphchi.jar "" "edu.cmu.graphchi.apps.${benchmark}" "${datadir}/${benchmark}.data 1"
    mkdir $benchmark
    mv chappie.graphchi/* $benchmark

    for file in ${benchmark}/*.*.0.csv; do
      mv $file ${file%.0.csv}.$i.csv
    done

    for file in ${benchmark}/*.*.0.txt; do
      mv $file ${file%.0.txt}.$i.txt
    done

    mv ${benchmark}/log.hpl ${benchmark}/log.${i}.hpl

    mkdir chappie.graphchibench.NOP/${benchmark}
    mv $benchmark/* chappie.graphchibench.NOP/${benchmark}/.
    rm -rf $benchmark
  done
done

export MODE=VM_SAMPLE
for benchmark in "${benchmarks[@]}"; do
  mkdir chappie.graphchibench/${benchmark}
  for i in $(seq 0 $iter); do
    ../chappie/run/run.sh graphchi.jar "" "edu.cmu.graphchi.apps.${benchmark}" "${datadir}/${benchmark}.data 1"
    mkdir $benchmark
    mv chappie.graphchi/* $benchmark

    for file in ${benchmark}/*.*.0.csv; do
      mv $file ${file%.0.csv}.$i.csv
    done

    for file in ${benchmark}/*.*.0.txt; do
      mv $file ${file%.0.txt}.$i.txt
    done

    mv ${benchmark}/log.hpl ${benchmark}/log.${i}.hpl

    mkdir chappie.graphchibench.SAMPLE/${benchmark}
    mv $benchmark/* chappie.graphchibench.SAMPLE/${benchmark}/.
    rm -rf $benchmark
  done
done

rm -rf chappie.graphchibench
mkdir chappie.graphchibench
mv chappie.graphchibench.* chappie.graphchibench/.

rm scratch -rf
