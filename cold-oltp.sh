#!/bin/bash

#rm -rf chappie.oltp.results
#mkdir chappie.oltp.results


export ITERS=1
export MODE=VM_SAMPLE
export MEMORY=false
export POLLING=4
export CORE_RATE=10
export READ_JIFFIES=true

iter=9

benchmarks=(
#tatp
#tpcc
#tpch
#noop
#smallbank
#twitter
ycsb
#epinions
)



for benchmark in $benchmarks; do
  mkdir chappie.oltp.results/${benchmark}
  for i in $(seq 0 $iter); do
    ../chappie/run/run.sh oltp.jar "" com.oltpbenchmark.DBWorkload  " -b ${benchmark} -c config/sample_${benchmark}_config.xml --execute=true -s 5 -o ${benchmark}.out" "-Dlog4j.configuration=log4j.properties"
    mkdir $benchmark
    mv chappie.oltp/* $benchmark

    for file in ${benchmark}/*.*.0.csv; do
      mv $file ${file%.0.csv}.$i.csv
    done

    for file in ${benchmark}/*.*.0.txt; do
      mv $file ${file%.0.txt}.$i.txt
    done

    mv ${benchmark}/log.hpl ${benchmark}/log.${i}.hpl

    mv $benchmark/* chappie.oltp.results/${benchmark}/.
    rm -rf $benchmark
  done
done
