#! /bin/bash
export ITERS=1
export MODE=VM_SAMPLE
export MEMORY=false
export POLLING=4
export CORE_RATE=10
export READ_JIFFIES=TRUE
iter=9


benchmarks=(
#sunflow,avrora
#sunflow,batik
#batik,avrora
#sunflow 
#avrora 
batik
#eclipse 
#h2 
#jython 
#luindex 
#lusearch 
#pmd 
#tomcat 
#tradebeans 
#tradesoap 
#xalan
#sunflow h2
)


#for benchmark in "${benchmarks[@]}"; do
#	mv ${benchmark}_${benchmark} dacapo_x_x
#done
#exit

if [ -d "dacapo_x_x" ]
then
  rm -r dacapo_x_x
fi

mkdir dacapo_x_x

for benchmark in "${benchmarks[@]}"; do

  echo "Runing $benchmark twice"
  if [[ "$benchmark" == *","* ]];
  then
    b1=$(echo $benchmark | cut -d "," -f 1)
    b2=$(echo $benchmark | cut -d "," -f 2)
    bash dacapo_2s_cold.sh $b1 $b2
    mv "${b1}_${b2}" dacapo_x_x
  else

    bash dacapo_2s_cold.sh $benchmark $benchmark
    mv "${benchmark}_${benchmark}" dacapo_x_x
  fi

done






#for benchmark in "${benchmarks[@]}"; do
#	mv ${benchmark}_${benchmark} dacapo_x_x
#done
