#/bin/bash

#ITER=$((COUNT))

ITER=0
LIMIT=10

#remove twitter files
#sudo rm -rf twitter-2010.txt.*
#sudo rm -rf twitter-2010.txt_*

while [ $ITER -ne $LIMIT ]
do
  echo ITER = $ITER
  echo LIMIT = $LIMIT
  mkdir chappie_iteration_$ITER
  mv chappie.thread.$ITER.csv chappie_iteration_$ITER/
  mv chappie_iteration_$ITER/chappie.thread.$ITER.csv chappie_iteration_$ITER/chappie.thread.data.csv
  echo *************ITERATION $ITER FINISHED******************
  ITER=$((ITER+1))

done

