#!/bin/bash

benchmarks=(
#  batik
#  sunflow 
  avrora 
#  eclipse 
#  h2 
#  jython 
#  luindex 
#  lusearch 
#  pmd 
#  tomcat 
  #tradebeans 
  #tradesoap 
#  xalan
)

if [ -d "dacapo_x_x" ]
then
rm -r dacapo_x_x
fi

mkdir dacapo_x_x

for benchmark in "${benchmarks[@]}"; do
	bash dacapo_2s.sh $benchmark $benchmark 
	#mv "${benchmark}_${benchmark}" dacapo_x_x
done


for benchmark in "${benchmarks[@]}"; do
	#bash dacapo_2s.sh $benchmark $benchmark 
	mv "${benchmark}_${benchmark}" dacapo_x_x
done

