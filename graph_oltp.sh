#!/bin/bash

PATH="./processed_data_oltp"
DEST="./processed_data_oltp"

benchmarks=(
#tatp
#tpcc
#tpch
#noop
#smallbank
twitter
#ycsb
#epinions
)


for benchmark in "${benchmarks[@]}"; do
  echo "Graphing $benchmark ..."

  /usr/bin/python3.5 ./scripting/method-energy-proportion.py -path="./processed_data/${benchmark}" -benchmark=${benchmark} -destination="./processed_data/graphs/${benchmark}/" -kind="java"

  /usr/bin/python3.5 ./scripting/cfa-method-energy-proportion.py -path="./processed_data/${benchmark}" -benchmark=${benchmark} -destination="./processed_data/graphs/${benchmark}/"

  #/usr/bin/python3.5 ./scripting/system_activeness_benchmark.py -path="./chappie.dacapo/${benchmark}/" -benchmark=${benchmark} -destination="./processed_data/graphs/${benchmark}/"
  #/usr/bin/python3.5 ./scripting/deep-method-energy-proportion.py -path="./processed_data/${benchmark}" -benchmark=${benchmark} -destination="./processed_data/graphs/${benchmark}/"
  #/usr/bin/python3.5 ./scripting/method-time-ranking.py -path="./processed_data/${benchmark}" -benchmark=${benchmark} -destination="./processed_data/graphs/${benchmark}/"
  #/usr/bin/python3.5 ./scripting/time_vs_energy.py -path="./processed_data/${benchmark}" -benchmark=${benchmark} -destination="./processed_data/graphs/${benchmark}/"
  #/usr/bin/python3.5 ./scripting/application-attribution-benchmark.py -path="./processed_data/${benchmark}" -benchmark=${benchmark} -destination="./processed_data/graphs/${benchmark}/"
  #/usr/bin/python3.5 ./scripting/physical-attribution-benchmark.py -path="./processed_data/${benchmark}" -benchmark=${benchmark} -destination="./processed_data/graphs/${benchmark}/"
  #/usr/bin/python3.5 ./scripting/system-application-attribution-benchmark.py -path="./processed_data/${benchmark}" -benchmark=${benchmark} -destination="./processed_data/graphs/${benchmark}/"
  #/usr/bin/python3.5 ./scripting/thread-movement-benchmark.py -path="./processed_data/${benchmark}/" -benchmark=${benchmark} -destination="./processed_data/graphs/${benchmark}/"

done
